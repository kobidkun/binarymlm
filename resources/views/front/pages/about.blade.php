@extends('front.base')
@section('content')
    <!-- Page Header -->
    <div class="page-header-section">
        <div class="container">
            <div class="row">
                <div class="page-header-area">
                    <div class="page-header-content">
                        <h2>About Us</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Header End -->

    <!-- About Us Section Start -->
    <section class="split section">
        <!-- Container Starts -->
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="images">
                        <img src="assets/img/about/plain-why-choose-us-2.png" alt="">
                    </div>
                </div>
                <div class="col-md-8 col-sm-6 col-xs-12">
                    <div class="content-inner">
                        <h2 class="title">Our Products is launched on 30th May 2019</h2>
                        <p class="lead">We are growing company dealing in Real-Estate, Transport's Services, Home Cleaning Products and many other Services. We are investing funds on these services and give huge amount of profit to our members.
                        </p>
                        <div class="details-list">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <h3>Minimal Coding</h3>
                                    <p>The design blocks come with ready to use HTML colde which makes the design kit suitable for developers and designers of all skill levels.</p>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <h3>Responsive</h3>
                                    <p>Your website will look good on any device. Each design block has been individually tested on desktop. tablets and smartphones.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container Ends -->
    </section>
    <!-- About Us Section Ends -->

    <section>
        <div class="container">
            <img src="{{asset('assets/img/2.png')}}" style="width:1200px;height:600px;">
        </div>
    </section>

@endsection