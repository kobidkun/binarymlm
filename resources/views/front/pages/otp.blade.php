@extends('front.base')
@section('content')



    <div class="page-header-section">
        <div class="container">
            <div class="row">
                <div class="page-header-area">
                    <div class="page-header-content">
                        <h2>Register</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Header End -->
    <style>
        input{
            padding: 10px;
            border: 1px solid #a2a2a2;
            width: 50px;
            height: 50px;
            text-align: center;
            font-size: 30px;
        }

    </style>
    <!-- About Us Section Start -->
    <section class="split section">
        <!-- Container Starts -->
        <div class="container">
            <div class="row" style="background-color: white">


                @if (Session::has('message'))
                    <div class="alert alert-danger">{{ Session::get('message') }}</div>
                @endif

                <div style="padding: 25px; background-color: #ffffff">
                    <form id="formid" action="{{route('front.otp.verify')}}" method="POST"  >


                        @csrf

                        <div style="padding: 35px">

                            <h4 >Enter 6 Digit OTP Sent to your Mobile</h4>

                            <a href="">Request a new OTP</a>


                        </div>


                        <div class="form-row">
                            <div class="form-group col-md-6">

                                <input type="hidden" value="{{$customer->id}}" name="customer_id"  />
                                <input type="hidden" id="otp" class="otp" name="otp" />
                                <input type="text" name="otp0" maxlength=1 id="1" onkeyup="moveOnMax(this,'a')" />
                                <input type="text" name="otp1"  maxlength=1 id="a" onkeyup="moveOnMax(this,'b')" />
                                <input type="text"  name="otp2" maxlength=1 id="b" onkeyup="moveOnMax(this,'c')" />
                                <input type="text"  name="otp3" maxlength=1 id="c" onkeyup="moveOnMax(this,'d')" />
                                <input type="text" name="otp4"  maxlength=1 id="d" onkeyup="moveOnMax(this,'e')" />
                                <input type="text" name="otp5"  maxlength=1 id="e" />
                            </div>

                        </div>




                        <button type="submit" class="btn btn-primary">Submit </button>






                    </form>


                </div>





            </div>
        </div>
    </section>



@endsection

@section('footer')

  <link rel="stylesheet" href="{{asset('js/css-toggle-switch-master/dist/toggle-switch.css')}}">

  <script src="{{asset('assets/js/jquery-min.js')}}"></script>

  <script>
      moveOnMax =function (field, nextFieldID) {
          if (field.value.length == 1) {
              document.getElementById(nextFieldID).focus();
          }
      }



  </script>


  <script>
      $(function() {


          $("#formid").on("keyup change", "input[type='text']", function () {

              var OTPVAL  = $("#1").val()+ $("#a").val()+ $("#b").val()+ $("#c").val()+ $("#d").val()+ $("#e").val();

              var OTPFIELD =  $('#otp');

              OTPFIELD.val(OTPVAL);

              console.log(OTPVAL);
          });

      });
  </script>



@stop
