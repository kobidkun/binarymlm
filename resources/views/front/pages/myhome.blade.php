@extends('front.base')
@section('content')


    <style>
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;

        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color:#3F5378;
            color: white;
        }

    </style>

    <!-- Service Block-1 Section -->
    <section id="service-block-main" class="section">
        <!-- Container Starts -->
        <div class="container">
            <div class="row">
                <h1 class="section-title wow fadeIn animated" data-wow-delay=".2s">
                    WHY CHOOSE US?
                </h1>
                <p class="section-subcontent">MLM or Multi Level Marketing is known as the procedure of marketing where a hierarchical method is mainly followed. The main aim of the MLM marketing professionals is not only to sell any products, but also to lure the clients to conduct the chain of selling the products further. This way the chain continues to grow. In this process, the profits of the initial salesperson increase constantly as the chain grows. Due to this reason, this procedure is also known as Network Marketing, Direct Marketing, Direct Sales, Direct Selling, Multi-Level Marketing or Matrix Marketing.</p>
                <div class="col-sm-6 col-md-3">
                    <!-- Service-Block-1 Item Starts -->
                    <div class="service-item wow fadeInUpQuick animated" data-wow-delay=".5s">
                        <div class="icon-wrapper">
                            <i class="icon-layers pulse-shrink">
                            </i>
                        </div>
                        <h2>
                            Bus Services
                        </h2>
                        <p>
                            With more than 12,000 bus routes spread across India and integration with over 1300 bus operators, Multimarktix's online bus reservation system is simpler and smarter. It provides you a wide range of facilities, right from choosing your pickup point to your preferred choice of seat (for instance, luxury buses with sleeper berths). You can also choose from the widest range of available buses like Mercedes, Volvo, Volvo AC, AC luxury, Deluxe, Sleeper, Express and other private buses
                        </p>
                    </div>
                    <!-- Service-Block-1 Item Ends -->
                </div>

                <div class="col-sm-6 col-md-3">
                    <!-- Service-Block-1 Item Starts -->
                    <div class="service-item wow fadeInUpQuick animated" data-wow-delay=".8s">
                        <div class="icon-wrapper">
                            <i class="icon-settings pulse-shrink">
                            </i>
                        </div>
                        <h2>
                            Taxi Services
                        </h2>
                        <p>
                           Book your cabs with India's leading online booking company since the year 2000. While booking cabs with Multimarktix, you can expect the ultimate online booking experience. With premium customer service, 24/7 dedicated helpline for support.
                        </p>
                    </div>
                    <!-- Service-Block-1 Item Ends -->
                </div>

                <div class="col-sm-6 col-md-3">
                    <!-- Service-Block-1 Item Starts -->
                    <div class="service-item wow fadeInUpQuick animated" data-wow-delay="1.1s">
                        <div class="icon-wrapper">
                            <i class="icon-energy pulse-shrink">
                            </i>
                        </div>
                        <h2>
                            Real Estate
                        </h2>
                        
                        <p>
                           
    Buying property - we deal in almost all the Sonipat, so if you want to buy a property in Sonipat, we are the right people.
     
    Selling property - we have a huge customer database, so if you want to sell your Property in Sonipat please contact us.

    Renting property - we also provide the facility of renting property, if you have a property to rent or want to take a property in rent, come to us.

                        </p>
                    </div>
                    <!-- Service-Block-1 Item Ends -->
                </div>

                <div class="col-sm-6 col-md-3">
                    <!-- Service-Block-1 Item Starts -->
                    <div class="service-item  wow fadeInUpQuick animated" data-wow-delay="1.4s">
                        <div class="icon-wrapper">
                            <i class="icon-cup pulse-shrink">
                            </i>
                        </div>
                        <h2>
                            Home Cleaning Products
                        </h2>
                        <p>
                           Discover the best cleaning supplies to make your home sparkle and shine. The Home Depot carries a large selection of cleaning supplies & tools for any job.
                        </p>
                    </div>
                </div><!-- Service-Block-1 Item Ends -->
            </div>
        </div><!-- Container Ends -->
    </section><!-- Service Main Section Ends -->

    <!-- About Us Section Start -->
    <section class="split section">
        <!-- Container Starts -->
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="images">
                        <img src="{{asset('assets/img/about/111.png')}}" alt="">
                    </div>
                </div>
                <div class="col-md-8 col-sm-6 col-xs-12">
                    <div class="content-inner">
                        <h2 class="title">CUSTOMER RELATIONS, MLM NETWORK</h2>
                        <p class="lead">We are growing company dealing in Real-Estate, Transport's Services, Home Cleaning Products and many other Services. We are investing funds on these services and give huge amount of profit to our members.
                        </p>
                        <div class="details-list">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <h3>Minimal Coding</h3>
                                    <p>The design blocks come with ready to use HTML colde which makes the design kit suitable for developers and designers of all skill levels.</p>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <h3>Responsive</h3>
                                    <p>Your website will look good on any device. Each design block has been individually tested on desktop. tablets and smartphones.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container Ends -->
    </section>
    <!-- About Us Section Ends -->

    <!-- Other Services Section -->
    <section id="other-services" class="section">
        <div class="container">
            <!-- Container Starts -->
            <div class="row">
                <h1 class="section-title wow fadeInUpQuick">
                    OUR SERVICES
                </h1>
<!--                <p class="section-subcontent">At vero eos et accusamus et iusto odio dignissimos ducimus qui <br> blanditiis praesentium</p>-->
                <!-- Other Service Item Wrapper Starts -->
                <div class="col-sm-6 col-md-6">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home" role="tab" aria-controls="home"><i class="icon-screen-desktop"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"><i class="icon-settings"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#messages" role="tab" aria-controls="messages"><i class="icon-heart"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#settings" role="tab" aria-controls="settings"><i class="icon-layers"></i></a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="home" role="tabpanel">
                            <div class="service-content wow fadeInUpQuick">
                                <h3>Bus Services</h3>
                                <p class="lead">The leading player in online Bus bookings in India, Multimarktix offers lowest fares, exclusive discounts and a seamless online booking experience. Desktop or mobile site is a delightfully customer friendly experience, and with just a few clicks you can complete your booking. </p>
                            </div>
                        </div>
                        <div class="tab-pane" id="profile" role="tabpanel">
                            <div class="service-content wow fadeInUpQuick">
                                <h3>Taxi Services </h3>
                                <p class="lead"> The leading player in online cab bookings in India, Multimarktix offers great offers on cab fares, exclusive discounts and a seamless online booking experience. Cabs, Bus, Flight, hotel and holiday bookings through the desktop or mobile site is a delightfully customer friendly experience, and with just a few clicks you can complete your booking.</p>
                            </div>
                        </div>
                        <div class="tab-pane" id="messages" role="tabpanel">
                            <div class="service-content fadeInUpQuick">
                                <h3>Real Estate Services</h3>
                                <p class="lead">Multimarktix is an online real estate advisor that functions on the fundamentals of trust, transparency and expertise. As a digital marketplace with an exhaustive range of property listings, we know it is easy to get lost. At Multimarktix.com, we guide home buyers right from the start of their home search to the very end. Browse through more than 139,000 verified real estate properties with accurate lowdown on amenities, neighborhoods and cities, and genuine pictures</p>
                            </div>
                        </div>
                        <div class="tab-pane" id="settings" role="tabpanel">
                            <div class="service-content fadeInUpQuick">
                                <h3>Home Cleaning Products</h3>
                                <p class="lead">Discover the best cleaning supplies to make your home sparkle and shine. The Home Depot carries a large selection of cleaning supplies & tools for any job. </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Other Service Item Wrapper Ends -->

                <!-- Porgress Strts -->
                <div class="col-sm-6 com-md-6">
                    <div class="skill-shortcode">
                        <div class="skill">
                            <p>
                                Bus Services 91%
                            </p>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar"  data-percentage="91">
                    <span class="sr-only">
                    91% Complete
                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="skill">
                            <p>
                                Taxi Services 86%
                            </p>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar"  data-percentage="86">
                    <span class="sr-only">
                    86% Complete
                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="skill">
                            <p>
                                Real Estate 78%
                            </p>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar"  data-percentage="78">
                    <span class="sr-only">
                    60% Complete
                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="skill">
                            <p>
                                Home Cleaning Products 65%
                            </p>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar"  data-percentage="65">
                    <span class="sr-only">
                    60% Complete
                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Other Service Image Ends-->
            </div>
        </div><!-- Container Ends -->
    </section>
    <!-- Other Services Section End -->

    <div id="free-promo" style="
    text-align: center;
    margin-top: 60px;
">

    </div>



    <div id="free-promo" style="
    text-align: center;
    margin-top: 60px;
">

    </div>

    {{--<!-- Team Section -->--}}
    {{--<section id="team" class="section">--}}
        {{--<!-- Container Starts -->--}}
        {{--<div class="container">--}}
            {{--<!-- Row Starts -->--}}
            {{--<div class="row">--}}
                {{--<h1 class="section-title wow fadeInDown" data-wow-delay=".5s">--}}
                    {{--MEET OUR TEAM--}}
                {{--</h1>--}}
                {{--<p class="section-subcontent">At vero eos et accusamus et iusto odio dignissimos ducimus qui <br> blanditiis praesentium</p>--}}
                {{--<div class="col-sm-6 col-md-3">--}}
                    {{--<!-- Team Item Starts -->--}}
                    {{--<div class="team-item wow fadeInUpQuick" data-wow-delay="1s">--}}
                        {{--<figure class="team-profile">--}}
                            {{--<img src="assets/img/team/team-01.jpg" alt="">--}}
                            {{--<figcaption class="our-team">--}}
                                {{--<div class="details">--}}
                                    {{--<p class="content-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>--}}
                                    {{--<div class="orange-line"></div>--}}
                                    {{--<div class="social">--}}
                                        {{--<a class="facebook" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a>--}}
                                        {{--<a class="twitter" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a>--}}
                                        {{--<a class="google-plus" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</figcaption>--}}
                        {{--</figure>--}}
                        {{--<div class="info">--}}
                            {{--<h2>--}}
                                {{--Sara smith--}}
                            {{--</h2>--}}
                            {{--<div class="orange-line"></div>--}}
                            {{--<p>--}}
                                {{--Founder And ceo--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- Team Item Ends -->--}}
                {{--</div>--}}

                {{--<div class="col-sm-6 col-md-3">--}}
                    {{--<!-- Team Item Starts -->--}}
                    {{--<div class="team-item wow fadeInUpQuick" data-wow-delay="1.4s">--}}
                        {{--<figure class="team-profile">--}}
                            {{--<img src="assets/img/team/team-02.jpg" alt="">--}}
                            {{--<figcaption class="our-team">--}}
                                {{--<div class="details">--}}
                                    {{--<p class="content-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>--}}
                                    {{--<div class="orange-line"></div>--}}
                                    {{--<div class="social">--}}
                                        {{--<a class="facebook" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a>--}}
                                        {{--<a class="twitter" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a>--}}
                                        {{--<a class="google-plus" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</figcaption>--}}
                        {{--</figure>--}}
                        {{--<div class="info">--}}
                            {{--<h2>--}}
                                {{--Sommer Christian--}}
                            {{--</h2>--}}
                            {{--<div class="orange-line"></div>--}}
                            {{--<p>--}}
                                {{--creative studio head--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</div><!-- Team Item Starts -->--}}
                {{--</div>--}}

                {{--<div class="col-sm-6 col-md-3">--}}
                    {{--<!-- Team Item Starts -->--}}
                    {{--<div class="team-item wow fadeInUpQuick" data-wow-delay="1.8s">--}}
                        {{--<figure class="team-profile">--}}
                            {{--<img src="assets/img/team/team-03.jpg" alt="">--}}
                            {{--<figcaption class="our-team">--}}
                                {{--<div class="details">--}}
                                    {{--<p class="content-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>--}}
                                    {{--<div class="orange-line"></div>--}}
                                    {{--<div class="social">--}}
                                        {{--<a class="facebook" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a>--}}
                                        {{--<a class="twitter" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a>--}}
                                        {{--<a class="google-plus" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</figcaption>--}}
                        {{--</figure>--}}
                        {{--<div class="info">--}}
                            {{--<h2>--}}
                                {{--Jane lupkin--}}
                            {{--</h2>--}}
                            {{--<div class="orange-line"></div>--}}
                            {{--<p>--}}
                                {{--magento developer--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- Team Item Ends -->--}}
                {{--</div>--}}

                {{--<div class="col-sm-6 col-md-3">--}}
                    {{--<!-- Team Item Starts -->--}}
                    {{--<div class="team-item wow fadeInUpQuick" data-wow-delay="2.2s">--}}
                        {{--<figure class="team-profile">--}}
                            {{--<img src="assets/img/team/team-04.jpg" alt="">--}}
                            {{--<figcaption class="our-team">--}}
                                {{--<div class="details">--}}
                                    {{--<p class="content-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>--}}
                                    {{--<div class="orange-line"></div>--}}
                                    {{--<div class="social">--}}
                                        {{--<a class="facebook" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a>--}}
                                        {{--<a class="twitter" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a>--}}
                                        {{--<a class="google-plus" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</figcaption>--}}
                        {{--</figure>--}}
                        {{--<div class="info">--}}
                            {{--<h2>--}}
                                {{--Sebastian roll--}}
                            {{--</h2>--}}
                            {{--<div class="orange-line"></div>--}}
                            {{--<p>--}}
                                {{--Logo / branding designer--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</div><!-- Team Item Ends -->--}}
                {{--</div>--}}
            {{--</div><!-- Row Ends -->--}}
        {{--</div><!-- Container Ends -->--}}
    {{--</section>--}}
    {{--<!-- Team Section End -->--}}

    <section>

        <div class="container">
            <img src="{{asset('assets/img/about/12.png')}}" style="width:1200px;height:150px;">
        </div>
        <div class="container">

            <table id="customers" style="margin-top: 5px">
                <tr>
                    <th>SR. NO.</th>
                    <th>INVESTMENT</th>
                    <th>R.O.I PER MONTH</th>
                    <th>TOTAL AMOUNT</th>
                    <th>MONTHS</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>6500</td>
                    <td>520</td>
                    <td>13000</td>
                    <td>25</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>12900</td>
                    <td>1290</td>
                    <td>25800</td>
                    <td>20</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>25800</td>
                    <td>3096</td>
                    <td>51600</td>
                    <td>17</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>51600</td>
                    <td>7224</td>
                    <td>103200</td>
                    <td>15</td>

                </tr>
                <tr>
                    <td>5</td>
                    <td>103200</td>
                    <td>16512</td>
                    <td>206400</td>
                    <td>13</td>
                </tr>

            </table>
        </div>
    </section>







    <div id="free-promo" style="
    text-align: center;
    margin-top: 60px;
">

    </div>

    {{--<!-- Clients Section -->--}}
    {{--<section id="clients" class="section">--}}
        {{--<!-- Container Ends -->--}}
        {{--<div class="container">--}}
            {{--<h1 class="section-title wow fadeInUpQuick" data-wow-delay=".5s">--}}
                {{--CLIENTS--}}
            {{--</h1>--}}
            {{--<p class="section-subcontent">At vero eos et accusamus et iusto odio dignissimos ducimus qui <br> blanditiis praesentium</p>--}}
            {{--<div class="wow fadeInUpQuick" data-wow-delay=".9s">--}}
                {{--<!-- Row and Scroller Wrapper Starts -->--}}
                {{--<div class="row" id="clients-scroller">--}}
                    {{--<div class="client-item-wrapper">--}}
                        {{--<img src="assets/img/clients/img1.png" alt="">--}}
                    {{--</div>--}}
                    {{--<div class="client-item-wrapper">--}}
                        {{--<img src="assets/img/clients/img2.png" alt="">--}}
                    {{--</div>--}}
                    {{--<div class="client-item-wrapper">--}}
                        {{--<img src="assets/img/clients/img3.png" alt="">--}}
                    {{--</div>--}}
                    {{--<div class="client-item-wrapper">--}}
                        {{--<img src="assets/img/clients/img4.png" alt="">--}}
                    {{--</div>--}}
                    {{--<div class="client-item-wrapper">--}}
                        {{--<img src="assets/img/clients/img5.png" alt="">--}}
                    {{--</div>--}}
                    {{--<div class="client-item-wrapper">--}}
                        {{--<img src="assets/img/clients/img6.png" alt="">--}}
                    {{--</div>--}}
                {{--</div><!-- Row and Scroller Wrapper Starts -->--}}
            {{--</div>--}}
        {{--</div><!-- Container Ends -->--}}
    {{--</section>--}}
    {{--<!-- Client Section End -->--}}
    <section>

        <div class="container">
            <img src="{{asset('assets/img/about/13.png')}}" style="width:1200px;height:150px;">
        </div>
        <div class="container">

            <table id="customers" style="margin-top: 5px; margin-bottom: 5px;"  >
                <tr>
                    <th>PAIR MATCHING</th>
                    <th>REWARDS</th>

                </tr>
                <tr>
                    <td>5 LAKH - 5 LAKH</td>
                    <td>LAPTOP &nbsp; <img src="{{asset('assets/img/about/lap.jpg')}}" style="width:60px;height:30px;"></td>

                </tr>
                <tr>
                    <td>10 LAKH - 10 LAKH</td>
                    <td>SPLENDER&nbsp; <img src="{{asset('assets/img/about/sp1.jpg')}}" style="width:60px;height:30px;"></td>

                </tr>
                <tr>
                    <td>20 LAKH - 20 LAKH</td>
                    <td>BULLET&nbsp;&nbsp;&nbsp;&nbsp; <img src="{{asset('assets/img/about/bull.png')}}" style="width:60px;height:30px;"></td>

                </tr>
                <tr>
                    <td>50 LAKH - 50 LAKH</td>
                    <td>TIAGO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src="{{asset('assets/img/about/tia.jpg')}}" style="width:60px;height:30px;"></td>


                </tr>
                <tr>
                    <td>70 LAKH - 70 LAKH</td>
                    <td>SWIFT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src="{{asset('assets/img/about/swi.png')}}" style="width:60px;height:30px;"></td>
                </tr>

                <tr>
                    <td>1.25 CRORE - 1.25 CRORE</td>
                    <td>BREZZA&nbsp;&nbsp;&nbsp; <img src="{{asset('assets/img/about/bre.png')}}" style="width:60px;height:30px;"></td>
                </tr>

                <tr>
                    <td>2 CRORE - 2 CRORE</td>
                    <td>HARRIER&nbsp;&nbsp; <img src="{{asset('assets/img/about/har.jpg')}}" style="width:60px;height:30px;"></td>
                </tr>

                <tr>
                    <td>3 CRORE - 3 CRORE</td>
                    <td>FORTUNER&nbsp; <img src="{{asset('assets/img/about/for.jpg')}}" style="width:60px;height:30px;"></td>
                </tr>


            </table>
        </div>
    </section>
    @endsection

<link rel="stylesheet" type="text/css" href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/css/all.css')}}">