@extends('front.base')
@section('content')

    <style>
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;

        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color:#3F5378;
            color: white;
        }

    </style>


    <!-- Page Header -->
    <div class="page-header-section">
        <div class="container">
            <div class="row">
                <div class="page-header-area">
                    <div class="page-header-content">
                        <h2>Returns</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Header End -->

    <!-- About Us Section Start -->
    <section class="split section">
        <!-- Container Starts -->
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="images">
                        <img src="{{asset('assets/img/about/plain-why-choose-us-2.png')}}" alt="">
                    </div>
                </div>
                <div class="col-md-8 col-sm-6 col-xs-12">
                    <div class="content-inner">
                        <h2 class="title">Our Products are launching on 30th May 2019</h2>
                        <p class="lead">We are growing company dealing in Real-Estate, Transport's Services, Home Cleaning Products, Entertainment Services and many other Services. We are investing funds on these services and give huge amount of profit to our members.
                        </p>
                        <div class="details-list">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <h3>Products</h3>
                                    <p>Multimarktix.com is a company that fullfill all your needs. We provides you home cleaning products and many more which we will launching soon to make Multimarktix your one stop solution.</p>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <h3>Services</h3>
                                    <p>MLM or Multi Level Marketing is known as the procedure of marketing where a hierarchical method is mainly followed. The main aim of the MLM marketing professionals is not only to sell any products, but also to lure the clients to conduct the chain of selling the products further. This way the chain continues to grow. In this process, the profits of the initial salesperson increase constantly as the chain grows. Due to this reason, this procedure is also known as Network Marketing, Direct Marketing, Direct Sales, Direct Selling, Multi-Level Marketing or Matrix Marketing.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container Ends -->
    </section>
    <!-- About Us Section Ends -->

    <div id="free-promo" style="
    text-align: center;
    margin-top: 60px;
">

    </div>


    <section>

        <div class="container">
            <img src="{{asset('assets/img/about/12.png')}}" style="width:1200px;height:150px;">
        </div>
        <div class="container">

            <table id="customers" style="margin-top: 5px">
                <tr>
                    <th>SR. NO.</th>
                    <th>INVESTMENT</th>
                    <th>R.O.I PER MONTH</th>
                    <th>TOTAL AMOUNT</th>
                    <th>MONTHS</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>6500</td>
                    <td>520</td>
                    <td>13000</td>
                    <td>25</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>12900</td>
                    <td>1290</td>
                    <td>25800</td>
                    <td>20</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>25800</td>
                    <td>3096</td>
                    <td>51600</td>
                    <td>17</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>51600</td>
                    <td>7224</td>
                    <td>103200</td>
                    <td>15</td>

                </tr>
                <tr>
                    <td>5</td>
                    <td>103200</td>
                    <td>16512</td>
                    <td>206400</td>
                    <td>13</td>
                </tr>

            </table>
        </div>
    </section>
    <div id="free-promo" style="
    text-align: center;
    margin-top: 60px;
">

    </div>
    <section>

        <div class="container">
            <img src="{{asset('assets/img/about/13.png')}}" style="width:1200px;height:150px;">
        </div>
        <div class="container">

            <table id="customers" style="margin-top: 5px; margin-bottom: 5px;"  >
                <tr>
                    <th>PAIR MATCHING</th>
                    <th>REWARDS</th>

                </tr>
                <tr>
                    <td>5 LAKH - 5 LAKH</td>
                    <td>LAPTOP &nbsp; <img src="{{asset('assets/img/about/lap.jpg')}}" style="width:60px;height:30px;"></td>

                </tr>
                <tr>
                    <td>10 LAKH - 10 LAKH</td>
                    <td>SPLENDER&nbsp; <img src="{{asset('assets/img/about/sp1.jpg')}}" style="width:60px;height:30px;"></td>

                </tr>
                <tr>
                    <td>20 LAKH - 20 LAKH</td>
                    <td>BULLET&nbsp;&nbsp;&nbsp;&nbsp; <img src="{{asset('assets/img/about/bull.png')}}" style="width:60px;height:30px;"></td>

                </tr>
                <tr>
                    <td>50 LAKH - 50 LAKH</td>
                    <td>TIAGO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src="{{asset('assets/img/about/tia.jpg')}}" style="width:60px;height:30px;"></td>


                </tr>
                <tr>
                    <td>70 LAKH - 70 LAKH</td>
                    <td>SWIFT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src="{{asset('assets/img/about/swi.png')}}" style="width:60px;height:30px;"></td>
                </tr>

                <tr>
                    <td>1.25 CRORE - 1.25 CRORE</td>
                    <td>BREZZA&nbsp;&nbsp;&nbsp; <img src="{{asset('assets/img/about/bre.png')}}" style="width:60px;height:30px;"></td>
                </tr>

                <tr>
                    <td>2 CRORE - 2 CRORE</td>
                    <td>HARRIER&nbsp;&nbsp; <img src="{{asset('assets/img/about/har.jpg')}}" style="width:60px;height:30px;"></td>
                </tr>

                <tr>
                    <td>3 CRORE - 3 CRORE</td>
                    <td>FORTUNER&nbsp; <img src="{{asset('assets/img/about/for.jpg')}}" style="width:60px;height:30px;"></td>
                </tr>


            </table>
        </div>
    </section>

@endsection

