@extends('front.base')
@section('content')



    <div class="page-header-section">
        <div class="container">
            <div class="row">
                <div class="page-header-area">
                    <div class="page-header-content">
                        <h2>Register</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Header End -->

    <!-- About Us Section Start -->
    <section class="split section">
        <!-- Container Starts -->
        <div class="container">
            <div class="row">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                    @if (Session::has('message'))
                        <div class="alert alert-danger">{{ Session::get('message') }}</div>
                    @endif








                    <form action="{{route('front.register.customer')}}" method="POST" >


                    @csrf

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">First Name</label>
                            <input type="text" class="form-control" name="fname" placeholder="First Name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Last Name</label>
                            <input type="text" class="form-control" name="lname" placeholder="Last Name">
                        </div>
                    </div>


                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputEmail4">Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Email">
                        </div>
                        

                        <div class="form-group col-md-4">
                            <label for="inputPassword4">Mobile</label>
                            <input type="text" class="form-control" name="mobile" placeholder="Mobile">
                        </div>
                    </div>


                    <div class="form-row">


                        <div class="form-group col-md-4">


                           {{--
                            <div class="input-group ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon3">MM0000</span>
                                </div>
                                <input type="text" class="form-control" name="sponser_id" id="sponcerid" placeholder="MM125699">
                            </div>--}}
                            <label for="basic-url">Sponcer ID</label>
                            <div class="form-group">


<div class="col-md-6">
    <input style="margin-right: -5px;position: absolute" type="text" class="form-control" readonly value="MM0000">
</div><div class="col-md-6">
                                    <input type="text" class="form-control" name="sponser_id" id="sponcerid" placeholder="1235">
</div>




                            </div>


                        </div>




                        <div class="form-group col-md-8">

                            <fieldset>
                                <label>Choose Leg</label>
                                <div class="switch-toggle switch-candy">
                                    <input id="week" name="hand" value="left" type="radio" checked>
                                    <label for="week" onclick="">Left</label>

                                    <input id="month" name="hand" value="right" type="radio">
                                    <label for="month" onclick="">Right</label>

                                    <a></a>
                                </div>
                            </fieldset>

                        </div>

                    </div>

                    <button type="submit" class="btn btn-primary btn-block">Submit </button>






                </form>




            </div>
        </div>
    </section>



@endsection

@section('footer')

  <link rel="stylesheet" href="{{asset('js/css-toggle-switch-master/dist/toggle-switch.css')}}">

  <script src="{{asset('assets/js/jquery-min.js')}}"></script>
  
   <link rel="stylesheet" href="https://service.emporiummarketing.com/plugin/ui-autocomplete/jquery-ui.min.css"/>
    <link rel="stylesheet" href="https://service.emporiummarketing.com/plugin/ui-autocomplete/jquery-ui.theme.min.css"/>
    <script src="https://service.emporiummarketing.com/plugin/ui-autocomplete/jquery-ui.min.js" type="text/javascript"></script>


    <script>
        $(document).ready(function () {


            var src = '{{route('front.api.search')}}';
            $("#sponcerid").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: src,
                        dataType: "json",
                        data: {
                            term: request.term
                        },
                        success: function (data) {
                            response(data);

                        }


                    });
                },
                minLength: 1,
                select: function (event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.ic_number);
                    //  $('.sponser--id').val(ui.item.sponser_id);

                    if(ui.item.id >= 1){

                        $(".registercustomer").removeAttr("disabled");

                    } else {
                        alert('Sponcer id not Available')
                    }










                }

            });
        });
    </script>

@stop
