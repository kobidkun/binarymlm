
<!-- DOCTYPE -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!-- Viewport Meta Tag -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome to {{env('APP_NAME')}}</title>


    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/main.css')}}">
    <!-- Responsive Style -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/responsive.css')}}">
    <!--Fonts-->
    <link rel="stylesheet" media="screen" href="{{asset('assets/fonts/font-awesome/font-awesome.min.css')}}">
    <link rel="stylesheet" media="screen" href="{{asset('assets/fonts/simple-line-icons.css')}}">

    <!-- Extras -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/extras/owl/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/extras/owl/owl.theme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/extras/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/extras/normalize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/extras/settings.css')}}">

    <!-- Color CSS Styles  -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/colors/mblue.css')}}" media="screen" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js">
    </script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js">
    </script>
    <![endif]-->
</head>
<body>

<!-- Header area wrapper starts -->
<header id="header-wrap">

    <!-- Roof area starts -->

    <!-- Roof area Ends -->

    <!-- Header area starts -->
 @include('front.components.header')

</header>
<!-- Header-wrap Section End -->

@yield('content')

@include('front.components.footer')

<!-- Go To Top Link -->
<a href="#" class="back-to-top">
    <i class="fa fa-angle-up">
    </i>
</a>



<!-- JavaScript & jQuery Plugins -->
<!-- jQuery Load -->
<script src="{{asset('assets/js/jquery-min.js')}}"></script>
<!-- Bootstrap JS -->
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<!--Text Rotator-->
<script src="{{asset('assets/js/jquery.mixitup.js')}}"></script>
<!--WOW Scroll Spy-->
<script src="{{asset('assets/js/wow.js')}}"></script>
<!-- OWL Carousel -->
<script src="{{asset('assets/js/owl.carousel.js')}}"></script>

<!-- WayPoint -->
<script src="{{asset('assets/js/waypoints.min.js')}}"></script>
<!-- CounterUp -->
<script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
<!-- ScrollTop -->
<script src="{{asset('assets/js/scroll-top.js')}}"></script>
<!-- Appear -->
<script src="{{asset('assets/js/jquery.appear.js')}}"></script>
<script src="{{asset('assets/js/jquery.vide.js')}}"></script>
<!-- All JS plugin Triggers -->
<script src="{{asset('assets/js/main.js')}}"></script>
@yield('footer')

</body>
</html>