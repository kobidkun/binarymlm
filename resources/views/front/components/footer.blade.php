<!-- Footer Section -->
<footer>
    <!-- Container Starts -->
    {{--<div class="container">--}}
        {{--<!-- Row Starts -->--}}
        {{--<div class="row section">--}}
            {{--<!-- Footer Widget Starts -->--}}
            {{--<div class="footer-widget col-md-3 col-xs-12 wow fadeIn">--}}
                {{--<h3 class="small-title">--}}
                    {{--ABOUT US--}}
                {{--</h3>--}}
                {{--<p>--}}
                    {{--Etiam ornare condimentum massa et scelerisque. Mauris nibh ipsum, laoreet at venenatis ac, rutrum sed risus,--}}
                {{--</p>--}}
                {{--<p>Aliquam magna nibh, mattis a urna nec. Semper venenatis magna.</p>--}}
                {{--<div class="social-footer">--}}
                    {{--<a href="#"><i class="fa fa-facebook icon-round"></i></a>--}}
                    {{--<a href="#"><i class="fa fa-twitter icon-round"></i></a>--}}
                    {{--<a href="#"><i class="fa fa-linkedin icon-round"></i></a>--}}
                    {{--<a href="#"><i class="fa fa-google-plus icon-round"></i></a>--}}
                {{--</div>--}}
            {{--</div><!-- Footer Widget Ends -->--}}

            {{--<!-- Footer Widget Starts -->--}}
            {{--<div class="footer-widget col-md-3 col-xs-12 wow fadeIn" data-wow-delay=".2s">--}}
                {{--<h3 class="small-title">--}}
                    {{--TWITTER--}}
                {{--</h3>--}}
                {{--<ul class="recent-tweets">--}}
                    {{--<li class="tweet">--}}
                        {{--My <a href="#">@Quora</a>--}}
                        {{--<span class="tweet-text">--}}
                  {{--answer to What's the best FAQ plugin for WordPress?--}}
                {{--</span>--}}
                        {{--<a href="#">qr.ae/RFTbIGa</a>--}}
                        {{--<span class="tweet-date">--}}
                  {{--August 21, 2015 10:29pm--}}
                {{--</span>--}}
                    {{--</li>--}}


                    {{--<li class="tweet">--}}
                        {{--WPB Advanced FAQ | Probably The Best WordPress FAQ Plugin <a href="#">wpbean.com/wpb-advanced-faq-pr…</a>--}}
                        {{--<span class="tweet-text">--}}
                  {{--via--}}
                {{--</span>--}}
                        {{--<a href="#">@wpbean</a>--}}
                        {{--<span class="tweet-date">--}}
                  {{--August 19, 2015 8:49 am--}}
                {{--</span>--}}
                    {{--</li>--}}


                {{--</ul>--}}
            {{--</div><!-- Footer Widget Ends -->--}}

            {{--<!-- Footer Widget Starts -->--}}
            {{--<div class="footer-widget col-md-3 col-xs-12 wow fadeIn" data-wow-delay=".5s">--}}
                {{--<h3 class="small-title">--}}
                    {{--GALLERY--}}
                {{--</h3>--}}
                {{--<div class="plain-flicker-gallery">--}}
                    {{--<a href="#" title="Pan Masala"><img src="assets/img/flicker/img1.jpg" alt=""></a>--}}
                    {{--<a href="#" title="Sports Template for Joomla"><img src="assets/img/flicker/img2.jpg" alt=""></a>--}}
                    {{--<a href="" title="Apple Keyboard"><img src="assets/img/flicker/img3.jpg" alt=""></a>--}}
                    {{--<a href="" title="Hard Working"><img src="assets/img/flicker/img4.jpg" alt=""></a>--}}
                    {{--<a href="" title="Smile"><img src="assets/img/flicker/img5.jpg" alt=""></a>--}}
                    {{--<a href="" title="Puzzle"><img src="assets/img/flicker/img6.jpg" alt=""></a>--}}
                {{--</div>--}}
            {{--</div><!-- Footer Widget Ends -->--}}

            {{--<!-- Footer Widget Starts -->--}}
            {{--<div class="footer-widget col-md-3 col-xs-12 wow fadeIn" data-wow-delay=".8s">--}}
                {{--<h3 class="small-title">--}}
                    {{--SUBSCRIBE US--}}
                {{--</h3>--}}

            {{--</div><!-- Footer Widget Ends -->--}}
        {{--</div><!-- Row Ends -->--}}
    {{--</div><!-- Container Ends -->--}}

    <!-- Copyright -->
    <div id="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <p class="copyright-text">
                        ©  2016 {{env('APP_NAME')}}. All right reserved.
                    </p>
                </div>
                <div class="col-md-6  col-sm-6">
                    <ul class="nav nav-inline pull-xs-right">
                        <li class="nav-item">
                            <a class="nav-link active" href="#">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Sitemap</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Privacy Policy</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Terms of services</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Copyright  End-->

</footer>
<!-- Footer Section End-->