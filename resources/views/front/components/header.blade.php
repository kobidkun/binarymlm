<section id="header">

    <!-- Navbar Starts -->
    <nav class="navbar navbar-light" data-spy="affix" data-offset-top="50">
        <div class="container">
            <button class='navbar-toggler hidden-md-up pull-xs-right' data-target='#main-menu' data-toggle='collapse' type='button'>
                ☰
            </button>
            <!-- Brand -->
            <a class="navbar-brand" href="{{route('front.home')}}">
                <img src="{{asset('assets/img/mm2.png')}}" alt="">
            </a>
            <div class="collapse navbar-toggleable-sm pull-xs-left pull-md-right" id="main-menu">
                <!-- Navbar Starts -->
                <ul class="nav nav-inline">
                    <li class="nav-item dropdown">
                        <a class="nav-link active" href="{{route('front.home')}}" role="button" aria-haspopup="true" aria-expanded="false">Home</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="{{route('front.about')}}" role="button" aria-haspopup="true" aria-expanded="false">
                            About Us
                        </a>
                        {{--<div class="dropdown-menu">--}}
                            {{--<a class="dropdown-item" href="about-us.html">About Us</a>--}}
                            {{--<a class="dropdown-item" href="about-us2.html">About Us 2</a>--}}
                            {{--<a class="dropdown-item" href="team-page.html">Team Members</a>--}}
                            {{--<a class="dropdown-item" href="services.html">Services</a>--}}
                            {{--<a class="dropdown-item" href="service2.html">Services 2</a>--}}
                            {{--<a class="dropdown-item" href="contact1.html">Contact Us</a>--}}
                            {{--<a class="dropdown-item" href="contact1.html">Contact Us 2</a>--}}
                            {{--<a class="dropdown-item" href="pricing.html">Pricing</a>--}}
                            {{--<a class="dropdown-item" href="404.html">404</a>--}}
                        {{--</div>--}}
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle"  href="{{route('front.returns')}}" role="button" aria-haspopup="true" aria-expanded="false">
                            Returns
                        </a>
                        {{--<div class="dropdown-menu">--}}
                            {{--<a class="dropdown-item" href="accordions.html">Accordions</a>--}}
                            {{--<a class="dropdown-item" href="tabs.html">Tabs</a>--}}
                            {{--<a class="dropdown-item" href="buttons.html">Buttons</a>--}}
                            {{--<a class="dropdown-item" href="skills.html">Progress Bars</a>--}}
                            {{--<a class="dropdown-item" href="testimonials.html">Testimonials</a>--}}
                            {{--<a class="dropdown-item" href="clients.html">Clients</a>--}}
                            {{--<a class="dropdown-item" href="icon.html">Icon Boxes</a>--}}
                            {{--<a class="dropdown-item" href="team.html">Team</a>--}}
                            {{--<a class="dropdown-item" href="carousel.html">Carousel</a>--}}
                            {{--<a class="dropdown-item" href="maps.html">Google Maps</a>--}}
                            {{--<a class="dropdown-item" href="pricing.html">Pricing tables</a>--}}
                            {{--<a class="dropdown-item" href="notification.html">Notification</a>--}}
                        {{--</div>--}}
                    </li>
                    {{--<li class="nav-item dropdown">--}}
                        {{--<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">--}}
                            {{--Portfolio--}}
                        {{--</a>--}}
                        {{--<div class="dropdown-menu">--}}
                            {{--<a class="dropdown-item" href="portfolio-col-2.html">Portfolio 2 Columns</a>--}}
                            {{--<a class="dropdown-item" href="portfolio-col-3.html">Portfolio 3 Columns</a>--}}
                            {{--<a class="dropdown-item" href="portfolio-col-4.html">Portfolio 4 Columns</a>--}}
                            {{--<a class="dropdown-item" href="portfolio-item.html">Portfolio Single</a>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="{{route('customer.login.view')}}" >
                            Log in
                        </a>
                        {{--<div class="dropdown-menu">--}}
                            {{--<a class="dropdown-item" href="sidebar-right.html">Sidebar Right</a>--}}
                            {{--<a class="dropdown-item" href="sidebar-left.html">Sidebar Left</a>--}}
                            {{--<a class="dropdown-item" href="sidebar-full.html">Full Width</a>--}}
                            {{--<a class="dropdown-item" href="blog-single.html">Single Post</a>--}}
                            {{--<a class="dropdown-item" href="blog-grids.html">Blog Grids</a>--}}
                        {{--</div>--}}
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle"  href="{{route('front.signup')}}">
                            Sign up
                        </a>
                        {{--<div class="dropdown-menu">--}}
                            {{--<a class="dropdown-item" href="contact1.html">Contact us 1</a>--}}
                            {{--<a class="dropdown-item" href="contact2.html">Contact us 2</a>--}}
                        {{--</div>--}}
                    </li>
                    {{--<!-- Search in right of nav -->--}}
                    {{--<li class="nav-item" class="search">--}}
                        {{--<form class="top_search clearfix">--}}
                            {{--<div class="top_search_con">--}}
                                {{--<input class="s" placeholder="Search Here ..." type="text">--}}
                                {{--<span class="top_search_icon"><i class="icon-magnifier"></i></span>--}}
                                {{--<input class="top_search_submit" type="submit">--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</li>--}}
                    {{--<!-- Search Ends -->--}}
                </ul>
            </div>
            <!-- Form for navbar search area -->

            <!-- Search form ends -->
        </div>
    </nav>
    <!-- Navbar Ends -->

</section>