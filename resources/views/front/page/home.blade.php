@extends('front.base')

@section('content')



<!-- Service Block-1 Section -->
<section id="service-block-main" class="section">
    <!-- Container Starts -->
    <div class="container">
        <div class="row">
            <h1 class="section-title wow fadeIn animated" data-wow-delay=".2s">
                WHY CHOOSE US?
            </h1>
            <p class="section-subcontent">At vero eos et accusamus et iusto odio dignissimos ducimus qui <br> blanditiis praesentium</p>
            <div class="col-sm-6 col-md-3">
                <!-- Service-Block-1 Item Starts -->
                <div class="service-item wow fadeInUpQuick animated" data-wow-delay=".5s">
                    <div class="icon-wrapper">
                        <i class="icon-layers pulse-shrink">
                        </i>
                    </div>
                    <h2>
                        40+ Pages
                    </h2>
                    <p>
                        Pellentesque ipsum erat, facilisis ut venenatis eu, sodales vel dolor.
                    </p>
                </div>
                <!-- Service-Block-1 Item Ends -->
            </div>

            <div class="col-sm-6 col-md-3">
                <!-- Service-Block-1 Item Starts -->
                <div class="service-item wow fadeInUpQuick animated" data-wow-delay=".8s">
                    <div class="icon-wrapper">
                        <i class="icon-settings pulse-shrink">
                        </i>
                    </div>
                    <h2>
                        Bootstrap4 and HTML5
                    </h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat Quidem!
                    </p>
                </div>
                <!-- Service-Block-1 Item Ends -->
            </div>

            <div class="col-sm-6 col-md-3">
                <!-- Service-Block-1 Item Starts -->
                <div class="service-item wow fadeInUpQuick animated" data-wow-delay="1.1s">
                    <div class="icon-wrapper">
                        <i class="icon-energy pulse-shrink">
                        </i>
                    </div>
                    <h2>
                        Refreshing Design
                    </h2>
                    <p>
                        Pellentesque ipsum erat, facilisis ut venenatis eu, sodales vel dolor.
                    </p>
                </div>
                <!-- Service-Block-1 Item Ends -->
            </div>

            <div class="col-sm-6 col-md-3">
                <!-- Service-Block-1 Item Starts -->
                <div class="service-item  wow fadeInUpQuick animated" data-wow-delay="1.4s">
                    <div class="icon-wrapper">
                        <i class="icon-cup pulse-shrink">
                        </i>
                    </div>
                    <h2>
                        Crafted Carefully
                    </h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat Quidem!
                    </p>
                </div>
            </div><!-- Service-Block-1 Item Ends -->
        </div>
    </div><!-- Container Ends -->
</section><!-- Service Main Section Ends -->

<!-- About Us Section Start -->
<section class="split section">
    <!-- Container Starts -->
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="images">
                    <img src="assets/img/about/plain-why-choose-us-2.png" alt="">
                </div>
            </div>
            <div class="col-md-8 col-sm-6 col-xs-12">
                <div class="content-inner">
                    <h2 class="title">BUILT-WITH BOOTSTRAP4, A NEW EXPRIENCE</h2>
                    <p class="lead">We Crafted an awesome design library that is robust and intuitive to use. No matter you're building a business presentation websit or a complex web application our design blocks can easily be adapted for your needs.</p>
                    <div class="details-list">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <h3>Minimal Coding</h3>
                                <p>The design blocks come with ready to use HTML colde which makes the design kit suitable for developers and designers of all skill levels.</p>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <h3>Responsive</h3>
                                <p>Your website will look good on any device. Each design block has been individually tested on desktop. tablets and smartphones.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container Ends -->
</section>
<!-- About Us Section Ends -->

<!-- Other Services Section -->
<section id="other-services" class="section">
    <div class="container">
        <!-- Container Starts -->
        <div class="row">
            <h1 class="section-title wow fadeInUpQuick">
                OUR SERVICES
            </h1>
            <p class="section-subcontent">At vero eos et accusamus et iusto odio dignissimos ducimus qui <br> blanditiis praesentium</p>
            <!-- Other Service Item Wrapper Starts -->
            <div class="col-sm-6 col-md-6">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#home" role="tab" aria-controls="home"><i class="icon-screen-desktop"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"><i class="icon-settings"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#messages" role="tab" aria-controls="messages"><i class="icon-heart"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#settings" role="tab" aria-controls="settings"><i class="icon-layers"></i></a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="home" role="tabpanel">
                        <div class="service-content wow fadeInUpQuick">
                            <h3>Responsive Design</h3>
                            <p class="lead">At vero eos et accusamus et <a href="#">iusto</a> odio digniss- <br> imos <b>ducimus</b> qui blanditiis praesentium voluptatum deleniti</p>
                            <p>quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. </p>
                        </div>
                    </div>
                    <div class="tab-pane" id="profile" role="tabpanel">
                        <div class="service-content wow fadeInUpQuick">
                            <h3>Very useful custom widget </h3>
                            <p class="lead">At vero eos et accusamus et <a href="#">iusto</a> odio digniss- <br> imos <b>ducimus</b> qui blanditiis praesentium voluptatum deleniti</p>
                            <p>quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. </p>
                        </div>
                    </div>
                    <div class="tab-pane" id="messages" role="tabpanel">
                        <div class="service-content fadeInUpQuick">
                            <h3>Graphic Design</h3>
                            <p class="lead">At vero eos et accusamus et <a href="#">iusto</a> odio digniss- <br> imos <b>ducimus</b> qui blanditiis praesentium voluptatum deleniti</p>
                            <p>quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. </p>
                        </div>
                    </div>
                    <div class="tab-pane" id="settings" role="tabpanel">
                        <div class="service-content fadeInUpQuick">
                            <h3>Parallax Background</h3>
                            <p class="lead">At vero eos et accusamus et <a href="#">iusto</a> odio digniss- <br> imos <b>ducimus</b> qui blanditiis praesentium voluptatum deleniti</p>
                            <p>quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Other Service Item Wrapper Ends -->

            <!-- Porgress Strts -->
            <div class="col-sm-6 com-md-6">
                <div class="skill-shortcode">
                    <div class="skill">
                        <p>
                            Web Design 91%
                        </p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar"  data-percentage="91">
                    <span class="sr-only">
                    91% Complete
                    </span>
                            </div>
                        </div>
                    </div>
                    <div class="skill">
                        <p>
                            HTML/CSS 86%
                        </p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar"  data-percentage="86">
                    <span class="sr-only">
                    86% Complete
                    </span>
                            </div>
                        </div>
                    </div>
                    <div class="skill">
                        <p>
                            Graphic Design 78%
                        </p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar"  data-percentage="78">
                    <span class="sr-only">
                    60% Complete
                    </span>
                            </div>
                        </div>
                    </div>
                    <div class="skill">
                        <p>
                            WordPress 65%
                        </p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar"  data-percentage="65">
                    <span class="sr-only">
                    60% Complete
                    </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Other Service Image Ends-->
        </div>
    </div><!-- Container Ends -->
</section>
<!-- Other Services Section End -->

<div id="free-promo" style="
    text-align: center;
    margin-top: 60px;
">
    <div class="container">
        <div class="row text-center">
            <div class="error-page">
                <h2><a rel="nofollow" href="https://rebrand.ly/gg-engage-purchase/">You are Using Free Version!<br> Purchase Full Version to Get All Pages and Features</a></h2>
                <a rel="nofollow" href="https://rebrand.ly/gg-engage-purchase/" class="btn btn-common btn-lg">Purchase Now</a>
            </div>
        </div>
    </div>
</div>

<!-- Featured Section Starts -->
<section id="featured" class="section">
    <!-- Container Starts -->
    <div class="container">
        <div class="row">
            <h1 class="section-title wow fadeInUpQuick">
                CORE FEATURES
            </h1>
            <p class="section-subcontent">At vero eos et accusamus et iusto odio dignissimos ducimus qui <br> blanditiis praesentium</p>
            <!-- Start Service Icon 1 -->
            <div class="col-md-4 col-sm-6" data-animation="fadeIn" data-animation-delay="01">
                <div class="featured-box">
                    <div class="featured-icon">
                        <i class="icon-present">
                        </i>
                    </div>
                    <div class="featured-content">
                        <h4>
                            Tons of shortcodes
                        </h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et magna aliqua.
                        </p>
                    </div>
                </div>
            </div>
            <!-- End featured Icon 1 -->
            <!-- Start featured Icon 1 -->
            <div class="col-md-4 col-sm-6" data-animation="fadeIn" data-animation-delay="01">
                <div class="featured-box">
                    <div class="featured-icon">
                        <i class="icon-rocket">
                        </i>
                    </div>
                    <div class="featured-content">
                        <h4>
                            Endless posibilites
                        </h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et magna aliqua.
                        </p>
                    </div>
                </div>
            </div>
            <!-- End featured Icon 1 -->
            <!-- Start featured Icon 1 -->
            <div class="col-md-4 col-sm-6" data-animation="fadeIn" data-animation-delay="01">
                <div class="featured-box">
                    <div class="featured-icon">
                        <i class="icon-pencil">
                        </i>
                    </div>
                    <div class="featured-content">
                        <h4>
                            24/7 Support
                        </h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et magna aliqua.
                        </p>
                    </div>
                </div>
            </div>
            <!-- End featured Icon 1 -->
            <!-- Start featured Icon 1 -->
            <div class="col-md-4 col-sm-6" data-animation="fadeIn" data-animation-delay="01">
                <div class="featured-box">
                    <div class="featured-icon">
                        <i class="icon-diamond icon-large icon-effect">
                        </i>
                    </div>
                    <div class="featured-content">
                        <h4>
                            Great Ideas
                        </h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et magna aliqua.
                        </p>
                    </div>
                </div>
            </div>
            <!-- End featured Icon 1 -->
            <!-- Start featured Icon 1 -->
            <div class="col-md-4 col-sm-6" data-animation="fadeIn" data-animation-delay="01">
                <div class="featured-box">
                    <div class="featured-icon">
                        <i class="icon-settings">
                        </i>
                    </div>
                    <div class="featured-content">
                        <h4>
                            Perfect Coding
                        </h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et magna aliqua.
                        </p>
                    </div>
                </div>
            </div>
            <!-- End featured Icon 1 -->
            <!-- Start featured Icon 1 -->
            <div class="col-md-4 col-sm-6" data-animation="fadeIn" data-animation-delay="01">
                <div class="featured-box">
                    <div class="featured-icon">
                        <i class="icon-star">
                        </i>
                    </div>
                    <div class="featured-content">
                        <h4>
                            Unique Design
                        </h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et magna aliqua.
                        </p>
                    </div>
                </div>
            </div>
            <!-- End featured Icon 1 -->
        </div>
    </div>
    <!-- Container Ends -->
</section>
<!-- Featured Section Ends -->

<div id="free-promo" style="
    text-align: center;
    margin-top: 60px;
">
    <div class="container">
        <div class="row text-center">
            <div class="error-page">
                <h2><a rel="nofollow" href="https://rebrand.ly/gg-engage-purchase/">You are Using Free Version!<br> Purchase Full Version to Get All Pages and Features</a></h2>
                <a rel="nofollow" href="https://rebrand.ly/gg-engage-purchase/" class="btn btn-common btn-lg">Purchase Now</a>
            </div>
        </div>
    </div>
</div>

<!-- Team Section -->
<section id="team" class="section">
    <!-- Container Starts -->
    <div class="container">
        <!-- Row Starts -->
        <div class="row">
            <h1 class="section-title wow fadeInDown" data-wow-delay=".5s">
                MEET OUR TEAM
            </h1>
            <p class="section-subcontent">At vero eos et accusamus et iusto odio dignissimos ducimus qui <br> blanditiis praesentium</p>
            <div class="col-sm-6 col-md-3">
                <!-- Team Item Starts -->
                <div class="team-item wow fadeInUpQuick" data-wow-delay="1s">
                    <figure class="team-profile">
                        <img src="assets/img/team/team-01.jpg" alt="">
                        <figcaption class="our-team">
                            <div class="details">
                                <p class="content-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                <div class="orange-line"></div>
                                <div class="social">
                                    <a class="facebook" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a>
                                    <a class="twitter" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a>
                                    <a class="google-plus" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="info">
                        <h2>
                            Sara smith
                        </h2>
                        <div class="orange-line"></div>
                        <p>
                            Founder And ceo
                        </p>
                    </div>
                </div>
                <!-- Team Item Ends -->
            </div>

            <div class="col-sm-6 col-md-3">
                <!-- Team Item Starts -->
                <div class="team-item wow fadeInUpQuick" data-wow-delay="1.4s">
                    <figure class="team-profile">
                        <img src="assets/img/team/team-02.jpg" alt="">
                        <figcaption class="our-team">
                            <div class="details">
                                <p class="content-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                <div class="orange-line"></div>
                                <div class="social">
                                    <a class="facebook" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a>
                                    <a class="twitter" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a>
                                    <a class="google-plus" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="info">
                        <h2>
                            Sommer Christian
                        </h2>
                        <div class="orange-line"></div>
                        <p>
                            creative studio head
                        </p>
                    </div>
                </div><!-- Team Item Starts -->
            </div>

            <div class="col-sm-6 col-md-3">
                <!-- Team Item Starts -->
                <div class="team-item wow fadeInUpQuick" data-wow-delay="1.8s">
                    <figure class="team-profile">
                        <img src="assets/img/team/team-03.jpg" alt="">
                        <figcaption class="our-team">
                            <div class="details">
                                <p class="content-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                <div class="orange-line"></div>
                                <div class="social">
                                    <a class="facebook" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a>
                                    <a class="twitter" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a>
                                    <a class="google-plus" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="info">
                        <h2>
                            Jane lupkin
                        </h2>
                        <div class="orange-line"></div>
                        <p>
                            magento developer
                        </p>
                    </div>
                </div>
                <!-- Team Item Ends -->
            </div>

            <div class="col-sm-6 col-md-3">
                <!-- Team Item Starts -->
                <div class="team-item wow fadeInUpQuick" data-wow-delay="2.2s">
                    <figure class="team-profile">
                        <img src="assets/img/team/team-04.jpg" alt="">
                        <figcaption class="our-team">
                            <div class="details">
                                <p class="content-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                <div class="orange-line"></div>
                                <div class="social">
                                    <a class="facebook" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a>
                                    <a class="twitter" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a>
                                    <a class="google-plus" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="info">
                        <h2>
                            Sebastian roll
                        </h2>
                        <div class="orange-line"></div>
                        <p>
                            Logo / branding designer
                        </p>
                    </div>
                </div><!-- Team Item Ends -->
            </div>
        </div><!-- Row Ends -->
    </div><!-- Container Ends -->
</section>
<!-- Team Section End -->

<div id="free-promo" style="
    text-align: center;
    margin-top: 60px;
">
    <div class="container">
        <div class="row text-center">
            <div class="error-page">
                <h2><a rel="nofollow" href="https://rebrand.ly/gg-engage-purchase/">You are Using Free Version!<br> Purchase Full Version to Get All Pages and Features</a></h2>
                <a rel="nofollow" href="https://rebrand.ly/gg-engage-purchase/" class="btn btn-common btn-lg">Purchase Now</a>
            </div>
        </div>
    </div>
</div>

<!-- Testimonial Section -->
<section id="testimonial" class="section">
    <!-- Container Starts -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div id="testimonial-item" class="owl-carousel">
                    <div class="item">
                        <div class="testimonial-inner">
                            <div class="testimonial-images">
                                <img class="img-circle" src="assets/img/testimonial/img1.jpg" alt="">
                            </div>
                            <div class="testimonial-content">
                                <p>
                                    Quisque mollis lacus augue, a hendrerit leo tristique vitae. Mauris non ipsum molestie sagittis elit ac vulputate odio.
                                </p>
                            </div>
                            <div class="testimonial-footer">
                                <i class="fa fa-quote-left"></i>
                                Steve Austin <a href="#">envato.com </a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-inner">
                            <div class="testimonial-images">
                                <img class="img-circle" src="assets/img/testimonial/img2.jpg" alt="">
                            </div>
                            <div class="testimonial-content">
                                <p>
                                    Quisque mollis lacus augue, a hendrerit leo tristique vitae. Mauris non ipsum molestie sagittis elit ac vulputate odio.
                                </p>
                            </div>
                            <div class="testimonial-footer">
                                <i class="fa fa-quote-left"></i>
                                Chelsey Siltanen<a href="#">Microsoft</a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-inner">
                            <div class="testimonial-images">
                                <img class="img-circle" src="assets/img/testimonial/img3.jpg" alt="">
                            </div>
                            <div class="testimonial-content">
                                <p>
                                    Quisque mollis lacus augue, a hendrerit leo tristique vitae. Mauris non ipsum molestie sagittis elit ac vulputate odio.
                                </p>
                            </div>
                            <div class="testimonial-footer">
                                <i class="fa fa-quote-left"></i>
                                Pamela Fox<a href="#">Khan Academy</a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-inner">
                            <div class="testimonial-images">
                                <img class="img-circle" src="assets/img/testimonial/img4.jpg" alt="">
                            </div>
                            <div class="testimonial-content">
                                <p>
                                    Quisque mollis lacus augue, a hendrerit leo tristique vitae. Mauris non ipsum molestie sagittis elit ac vulputate odio.
                                </p>
                            </div>
                            <div class="testimonial-footer">
                                <i class="fa fa-quote-left"></i>
                                janna Hagan<a href="#">Google</a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-inner">
                            <div class="testimonial-images">
                                <img class="img-circle" src="assets/img/testimonial/img5.jpg" alt="">
                            </div>
                            <div class="testimonial-content">
                                <p>
                                    Quisque mollis lacus augue, a hendrerit leo tristique vitae. Mauris non ipsum molestie sagittis elit ac vulputate odio.
                                </p>
                            </div>
                            <div class="testimonial-footer">
                                <i class="fa fa-quote-left"></i>
                                Paul Tweedy<a href="#">BBC</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Container Ends -->
</section>
<!-- Testimonial Section End -->

<div id="free-promo" style="
    text-align: center;
    margin-top: 60px;
">
    <div class="container">
        <div class="row text-center">
            <div class="error-page">
                <h2><a rel="nofollow" href="https://rebrand.ly/gg-engage-purchase/">You are Using Free Version!<br> Purchase Full Version to Get All Pages and Features</a></h2>
                <a rel="nofollow" href="https://rebrand.ly/gg-engage-purchase/" class="btn btn-common btn-lg">Purchase Now</a>
            </div>
        </div>
    </div>
</div>

<!-- Clients Section -->
<section id="clients" class="section">
    <!-- Container Ends -->
    <div class="container">
        <h1 class="section-title wow fadeInUpQuick" data-wow-delay=".5s">
            CLIENTS
        </h1>
        <p class="section-subcontent">At vero eos et accusamus et iusto odio dignissimos ducimus qui <br> blanditiis praesentium</p>
        <div class="wow fadeInUpQuick" data-wow-delay=".9s">
            <!-- Row and Scroller Wrapper Starts -->
            <div class="row" id="clients-scroller">
                <div class="client-item-wrapper">
                    <img src="assets/img/clients/img1.png" alt="">
                </div>
                <div class="client-item-wrapper">
                    <img src="assets/img/clients/img2.png" alt="">
                </div>
                <div class="client-item-wrapper">
                    <img src="assets/img/clients/img3.png" alt="">
                </div>
                <div class="client-item-wrapper">
                    <img src="assets/img/clients/img4.png" alt="">
                </div>
                <div class="client-item-wrapper">
                    <img src="assets/img/clients/img5.png" alt="">
                </div>
                <div class="client-item-wrapper">
                    <img src="assets/img/clients/img6.png" alt="">
                </div>
            </div><!-- Row and Scroller Wrapper Starts -->
        </div>
    </div><!-- Container Ends -->
</section>
<!-- Client Section End -->


@stop