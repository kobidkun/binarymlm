<!-- Header area wrapper starts -->
<header id="header-wrap">

    <!-- Roof area starts -->

    <!-- Roof area Ends -->

    <!-- Header area starts -->
    <section id="header">

        <!-- Navbar Starts -->
        <nav class="navbar navbar-light" data-spy="affix" data-offset-top="50">
            <div class="container">
                <button class='navbar-toggler hidden-md-up pull-xs-right' data-target='#main-menu' data-toggle='collapse' type='button'>
                    ☰
                </button>
                <!-- Brand -->
                <a class="navbar-brand" href="index.html">
                    <img src="assets/img/logo.png" alt="">
                </a>
                <div class="collapse navbar-toggleable-sm pull-xs-left pull-md-right" id="main-menu">
                    <!-- Navbar Starts -->
                    <ul class="nav nav-inline">
                        <li class="nav-item dropdown">
                            <a class="nav-link active" href="index.html" role="button" aria-haspopup="true" aria-expanded="false">Home</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                Pages
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="about-us.html">About Us</a>

                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                Shortcodes
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="accordions.html">Accordions</a>

                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                Portfolio
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="portfolio-col-2.html">Portfolio 2 Columns</a>
                                <a class="dropdown-item" href="portfolio-col-3.html">Portfolio 3 Columns</a>
                                <a class="dropdown-item" href="portfolio-col-4.html">Portfolio 4 Columns</a>
                                <a class="dropdown-item" href="portfolio-item.html">Portfolio Single</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                Blog
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="sidebar-right.html">Sidebar Right</a>
                                <a class="dropdown-item" href="sidebar-left.html">Sidebar Left</a>
                                <a class="dropdown-item" href="sidebar-full.html">Full Width</a>
                                <a class="dropdown-item" href="blog-single.html">Single Post</a>
                                <a class="dropdown-item" href="blog-grids.html">Blog Grids</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link "
                              href="{{route('customer.login.view')}}"
                               role="button" aria-haspopup="true" aria-expanded="false">
                                Login
                            </a>

                        </li>
                        <!-- Search in right of nav -->

                        <!-- Search Ends -->
                    </ul>
                </div>
                <!-- Form for navbar search area -->
                <form class="full-search">
                    <div class="container">
                        <input type="text" placeholder="Type to Search">
                        <a href="#" class="close-search">
                    <span class="fa fa-times fa-2x">
                    </span>
                        </a>
                    </div>
                </form>
                <!-- Search form ends -->
            </div>
        </nav>
        <!-- Navbar Ends -->

    </section>
    <!-- Start Content -->
    <div id="free-promo" style="
    text-align: center;
    margin-top: 60px;
">
        <div class="container">
            <div class="row text-center">
                <div class="error-page">
                    <h2><a rel="nofollow" href="https://rebrand.ly/gg-engage-purchase/">You are Using Free Version!<br> Purchase Full Version to Get All Pages and Features</a></h2>
                    <a rel="nofollow" href="https://rebrand.ly/gg-engage-purchase/" class="btn btn-common btn-lg">Purchase Now</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End Content -->
</header>
<!-- Header-wrap Section End -->