<!-- Vertical Nav -->
<nav class="hk-nav hk-nav-light">
    <a href="javascript:void(0);" id="hk_nav_close" class="hk-nav-close"><span class="feather-icon"><i data-feather="x"></i></span></a>
    <div class="nicescroll-bar">
        <div class="navbar-nav-wrap">
            <ul class="navbar-nav flex-column">






                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('customer.dashboard')}}" data-target="#dash_drp">
                            <span class="feather-icon"><i data-feather="activity"></i></span>
                            <span class="nav-link-text">Dashboard</span>
                        </a>



                    </li>


                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('customer.tree.view')}}" data-target="#dash_drp">
                            <span class="feather-icon"><i data-feather="user"></i></span>
                            <span class="nav-link-text">Tree View</span>
                        </a>



                    </li>


                    <li class="nav-item">
                        <a class="nav-link link-with-badge" href="javascript:void(0);"

                           data-toggle="collapse" data-target="#app_temp">
                            <span class="feather-icon"><i data-feather="users"></i></span>
                            <span class="nav-link-text">Profile</span>

                        </a>
                        <ul id="app_temp" class="nav flex-column collapse collapse-level-1">
                            <li class="nav-item">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('customer.profile.edit')}}">Edit</a>
                                    </li>


                                </ul>
                            </li>
                        </ul>
                    </li>
                <li class="nav-item">
                    <a class="nav-link link-with-badge" href="javascript:void(0);"

                       data-toggle="collapse" data-target="#app_tempp">
                        <span class="feather-icon"><i data-feather="users"></i></span>
                        <span class="nav-link-text">Payout</span>

                    </a>
                    <ul id="app_tempp" class="nav flex-column collapse collapse-level-1">
                        <li class="nav-item">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('customer.payout.create')}}">Create request</a>
                                </li>


                            </ul>
                        </li>
                    </ul>
                </li>







            </ul>





        </div>
    </div>
</nav>
<div id="hk_nav_backdrop" class="hk-nav-backdrop"></div>
<!-- /Vertical Nav -->