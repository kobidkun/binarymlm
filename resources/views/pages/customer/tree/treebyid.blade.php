@extends('base')

@section('content')
    <link rel="stylesheet" href="{{asset('/js/vis/dist/vis.min.css') }}" />
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">



                <!--end::Section-->


                @if ($customer->plan  === null)
                    <div class="container">
                        <!-- Title -->

                        <!-- /Title -->

                        <!-- Row -->
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="alert alert-danger" role="alert">
                                    Your Profile Not active Please Make payment And Activate Plan
                                </div>

                            </div>
                        </div>


                    </div>

                @endif


                <style>
                    * {margin: 0; padding: 0;}

                    .tree ul {
                        padding-top: 20px; position: relative;

                        transition: all 0.5s;
                        -webkit-transition: all 0.5s;
                        -moz-transition: all 0.5s;
                    }

                    .tree li {
                        float: left; text-align: center;
                        list-style-type: none;
                        position: relative;
                        padding: 20px 5px 0 5px;

                        transition: all 0.5s;
                        -webkit-transition: all 0.5s;
                        -moz-transition: all 0.5s;
                    }

                    /*We will use ::before and ::after to draw the connectors*/

                    .tree li::before, .tree li::after{
                        content: '';
                        position: absolute; top: 0; right: 50%;
                        border-top: 1px solid #ccc;
                        width: 50%; height: 20px;
                    }
                    .tree li::after{
                        right: auto; left: 50%;
                        border-left: 1px solid #ccc;
                    }

                    /*We need to remove left-right connectors from elements without
                    any siblings*/
                    .tree li:only-child::after, .tree li:only-child::before {
                        display: none;
                    }

                    /*Remove space from the top of single children*/
                    .tree li:only-child{ padding-top: 0;}

                    /*Remove left connector from first child and
                    right connector from last child*/
                    .tree li:first-child::before, .tree li:last-child::after{
                        border: 0 none;
                    }
                    /*Adding back the vertical connector to the last nodes*/
                    .tree li:last-child::before{
                        border-right: 1px solid #ccc;
                        border-radius: 0 5px 0 0;
                        -webkit-border-radius: 0 5px 0 0;
                        -moz-border-radius: 0 5px 0 0;
                    }
                    .tree li:first-child::after{
                        border-radius: 5px 0 0 0;
                        -webkit-border-radius: 5px 0 0 0;
                        -moz-border-radius: 5px 0 0 0;
                    }

                    /*Time to add downward connectors from parents*/
                    .tree ul ul::before{
                        content: '';
                        position: absolute; top: 0; left: 50%;
                        border-left: 1px solid #ccc;
                        width: 0; height: 20px;
                    }

                    .tree li a{
                        border: 1px solid #ccc;
                        padding: 5px 10px;
                        text-decoration: none;
                        color: #666;
                        font-family: arial, verdana, tahoma;
                        font-size: 11px;
                        display: inline-block;

                        border-radius: 5px;
                        -webkit-border-radius: 5px;
                        -moz-border-radius: 5px;

                        transition: all 0.5s;
                        -webkit-transition: all 0.5s;
                        -moz-transition: all 0.5s;
                    }

                    /*Time for some hover effects*/
                    /*We will apply the hover effect the the lineage of the element also*/
                    .tree li div a:hover, .tree li div a:hover+ul li div a {
                        background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
                    }
                    /*Connector styles on hover*/
                    .tree li div a:hover+ul li::after,
                    .tree li div a:hover+ul li::before,
                    .tree li div a:hover+ul::before,
                    .tree li div a:hover+ul ul::before{
                        border-color:  #94a0b4;
                    }

                    /*Thats all. I hope you enjoyed it.
                    Thanks :)*/
                </style>


                <div class="row">
                    <div class="col-md-12">




                        <div class="tree">
                            <ul>
                                <li>


                                    <div class="parent">



                                    </div>


                                    <ul>


                                        {{-- 1--}}


                                        <li>
                                            <div class="child-1">


                                            </div>


                                            <ul>

                                                {{-- 1 1--}}

                                                <li>

                                                    <div class="child-1-1">



                                                    </div>

                                                    <ul>

                                                        {{-- 1 1 1 --}}
                                                        <li>

                                                            <div class="1-1-1">




                                                            </div>

                                                        </li>

                                                        <li>


                                                            <div class="1-1-2">




                                                            </div>

                                                        </li>
                                                    </ul>


                                                </li>

                                                <li>

                                                    <div class="child-1-2">



                                                    </div>

                                                    <ul>

                                                        {{-- 1 1 1 --}}
                                                        <li>

                                                            <div class="child-1-2-1">




                                                            </div>

                                                        </li>

                                                        <li>


                                                            <div class="child-1-2-2">




                                                            </div>

                                                        </li>
                                                    </ul>


                                                </li>
                                            </ul>
                                        </li>



                                        <li>

                                            <div class="child-2">



                                            </div>

                                            <ul>
                                                <li>

                                                    <div class="child-2-1">


                                                    </div>

                                                    <ul>
                                                        <li>

                                                            <div class="child-2-1-1">


                                                            </div>

                                                        </li>

                                                        <li>

                                                            <div class="child-2-1-2">



                                                            </div>

                                                        </li>
                                                    </ul>




                                                </li>

                                                <li>

                                                    <div class="child-2-2">



                                                    </div>


                                                    <ul>
                                                        <li>

                                                            <div class="child-2-2-1">

                                                            </div>

                                                        </li>

                                                        <li>

                                                            <div class="child-2-2-2">



                                                            </div>

                                                        </li>
                                                    </ul>

                                                </li>
                                            </ul>
                                        </li>




                                    </ul>
                                </li>
                            </ul>
                        </div>



                    </div>
                </div>







            </div>


        </div>
    </div>
    </div>

@endsection



@section('footer')


    <style>
        .parent-css {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            max-width: 220px;
            margin: auto;
            text-align: center;

            width: 100%;

            padding: 10px;
            background-color: #ffffff;
        }

        .title {
            color: grey;
            font-size: 10px;
        }

        .a555 {
            border: none;
            outline: 0;
            display: inline-block;
            padding: 8px;
            color: white;
            background-color: #000;
            text-align: center;
            cursor: pointer;
            width: 100%;
            font-size: 12px;
        }

        a {
            text-decoration: none;
            font-size: 12px;
            color: black;
        }

        button:hover, a:hover {
            opacity: 0.7;
        }
    </style>

    <script>






        var downline = [];

        $.ajax({
            /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
            url:"{{route('customer.tree.view.id',$id)}}",
            //  async: true,
            dataType: 'json',

            success: function(obj){
                // var json = $.parseJSON(obj);

                $(".parent").replaceWith('<div class=" parent-css">\n' +
                    '  <i style="font-size: 128px" class="fa fa-user"></i>\n' +
                    '  <h4>'+obj.customer.fname +' '+obj.customer.lname+'</h4>\n' +
                    '  <p class="title"> IC Number: MM-0000'+obj.customer.ic_number +'</p>\n' +
                    '  <p class="title"> Sponser Id: MM-0000'+obj.customer.sponser_id +'</p>\n' +
                    '  <p class="title"> Plan: '+obj.customer.plan +'</p>\n' +
                    '  <p class="title"> Phone: '+obj.customer.phone +'</p>\n' +
                    '  <p class="title"> Hand: '+obj.customer.hand +'</p>\n' +

                    '</div>')



                //    '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'




                if (obj.left !== null) {

                    $(".child-1").append('<div class="parent-css ">\n' +
                        '  <i style="font-size: 128px" class="fa fa-user"></i>\n' +
                        '  <h6 style="color: #5f4cff;">First Generation</h6>\n' +
                        '  <h4>'+obj.left.fname +' '+obj.left.lname+'</h4>\n' +
                        '  <p class="title"> IC Number: MM-0000'+obj.left.ic_number +'</p>\n' +
                        '  <p class="title"> Sponser Id: MM-0000'+obj.left.sponser_id +'</p>\n' +
                        '  <p class="title"> Plan: '+obj.left.plan +'</p>\n' +
                        '  <p class="title"> Phone: '+obj.left.phone +'</p>\n' +
                        '  <p class="title"> Hand: '+obj.left.hand +'</p>\n' +
                        '  <p><a class="btn btn-primary" href="/customer/tree-view-by-id/'+obj.left.id+'">View More Downline</a></p>\n' +
                        '</div>');


                    if (obj.left.left_id !== null) {


                        $.ajax({
                            /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
                            url: '/customer/tree-view/'+obj.left.id,
                            //  async: true,
                            dataType: 'json',

                            success: function (obj) {


                                if (obj.left !== null) {


                                    $(".child-1-1").append('<div class="parent-css">\n' +
                                        '  <i style="font-size: 128px" class="fa fa-user"></i>\n' +
                                        '  <h6 style="color: #fff651;">Second Generation</h6>\n' +
                                        '  <h4>' + obj.left.fname + ' ' + obj.left.lname + '</h4>\n' +
                                        '  <p class="title"> IC Number: MM-0000' + obj.left.ic_number + '</p>\n' +
                                        '  <p class="title"> Sponser Id: MM-0000' + obj.left.sponser_id + '</p>\n' +
                                        '  <p class="title"> Plan: ' + obj.left.plan + '</p>\n' +
                                        '  <p class="title"> Phone: ' + obj.left.phone + '</p>\n' +
                                        '  <p class="title"> Hand: ' + obj.left.hand + '</p>\n' +
                                        '  <p><a class="btn btn-primary" href="/customer/tree-view-by-id/'+obj.left.id+'">View More Downline</a></p>\n' +
                                        '</div>');

                                }


                                if (obj.right !== null) {

                                    $(".child-1-2").append('<div class="parent-css">\n' +
                                        '  <i style="font-size: 128px" class="fa fa-user"></i>\n' +
                                        '  <h6 style="color: #fff651;">Second Generation</h6>\n' +
                                        '  <h4>' + obj.right.fname + ' ' + obj.right.lname + '</h4>\n' +
                                        '  <p class="title"> IC Number: MM-0000' + obj.right.ic_number + '</p>\n' +
                                        '  <p class="title"> Sponser Id: MM-0000' + obj.right.sponser_id + '</p>\n' +
                                        '  <p class="title"> Plan: ' + obj.right.plan + '</p>\n' +
                                        '  <p class="title"> Phone: ' + obj.right.phone + '</p>\n' +
                                        '  <p class="title"> Hand: ' + obj.right.hand + '</p>\n' +
                                        '  <p><a class="btn btn-primary" href="/customer/tree-view-by-id/'+obj.right.id+'">View More Downline</a></p>\n' +
                                        '</div>');



                                }





                            }

                        })


                    }







                }


                if (obj.right !== null) {

                    $(".child-2").replaceWith('<div class="parent-css ">\n' +
                        '  <i style="font-size: 128px" class="fa fa-user"></i>\n' +
                        '  <h6 style="color: #5f4cff;">First Generation</h6>\n' +
                        '  <h4>'+obj.right.fname +' '+obj.right.lname+'</h4>\n' +
                        '  <p class="title"> IC Number: MM-0000'+obj.right.ic_number +'</p>\n' +
                        '  <p class="title"> Sponser Id: MM-0000'+obj.right.sponser_id +'</p>\n' +
                        '  <p class="title"> Plan: '+obj.right.plan +'</p>\n' +
                        '  <p class="title"> Phone: '+obj.right.phone +'</p>\n' +
                        '  <p class="title"> Hand: '+obj.right.hand +'</p>\n' +
                        '  <p><a class="btn btn-primary" href="/customer/tree-view-by-id/'+obj.right.id+'">View More Downline</a></p>\n' +
                        '</div>');




                    if (obj.right.left_id !== null) {


                        $.ajax({
                            /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
                            url: '/customer/tree-view/'+obj.right.id,
                            //  async: true,
                            dataType: 'json',

                            success: function (obj) {


                                if (obj.left !== null) {


                                    $(".child-2-1").append('<div class="parent-css">\n' +
                                        '  <i style="font-size: 128px" class="fa fa-user"></i>\n' +
                                        '  <h6 style="color: #fff651;">Second Generation</h6>\n' +
                                        '  <h4>' + obj.left.fname + ' ' + obj.left.lname + '</h4>\n' +
                                        '  <p class="title"> IC Number: MM-0000' + obj.left.ic_number + '</p>\n' +
                                        '  <p class="title"> Sponser Id: MM-0000' + obj.left.sponser_id + '</p>\n' +
                                        '  <p class="title"> Plan: ' + obj.left.plan + '</p>\n' +
                                        '  <p class="title"> Phone: ' + obj.left.phone + '</p>\n' +
                                        '  <p class="title"> Hand: ' + obj.left.hand + '</p>\n' +
                                        '  <p><a class="btn btn-primary" href="/customer/tree-view-by-id/'+obj.left.id+'">View More Downline</a></p>\n' +
                                        '</div>');

                                }


                                if (obj.right !== null) {

                                    $(".child-2-2").append('<div class="parent-css">\n' +
                                        '  <i style="font-size: 128px" class="fa fa-user"></i>\n' +
                                        '  <h6 style="color: #fff651;">Second Generation</h6>\n' +
                                        '  <h4>' + obj.right.fname + ' ' + obj.right.lname + '</h4>\n' +
                                        '  <p class="title"> IC Number: MM-0000' + obj.right.ic_number + '</p>\n' +
                                        '  <p class="title"> Sponser Id: MM-0000' + obj.right.sponser_id + '</p>\n' +
                                        '  <p class="title"> Plan: ' + obj.right.plan + '</p>\n' +
                                        '  <p class="title"> Phone: ' + obj.right.phone + '</p>\n' +
                                        '  <p class="title"> Hand: ' + obj.right.hand + '</p>\n' +
                                        '  <p><a class="btn btn-primary" href="/customer/tree-view-by-id/'+obj.right.id+'">View More Downline</a></p>\n' +
                                        '</div>');



                                }





                            }

                        })


                    }
                }












            },
            error: function(error){
                alert(error);
            }
        })











    </script>

@endsection

