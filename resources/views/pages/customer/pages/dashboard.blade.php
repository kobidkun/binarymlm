@extends('pages.customer.base')

@section('content')




        <!-- Container -->
        <div class="container mt-xl-50 mt-sm-30 mt-15">


            <!-- Row -->
            <div class="row">
                <div class="col-xl-12">


                    <div class="hk-row">
                        <div class="col-sm-12">
                            <p style="color: green"> Dashboard updates Every 15th and last day of every month</p>



                            @if (\Carbon\Carbon::now()->day > 15)
                                <p style="color: red"> Next Update: {{\Carbon\Carbon::now()->endOfMonth()->day}}th

                                    of this month
                                </p>

                                @else

                                <p style="color: red">    Next Update: 15th of this month

                            @endif



                            <p style="color: red"> Current Date: {{\Carbon\Carbon::now()}}</p>
                            <div class="card-group hk-dash-type-2">

                                <div class="card card-sm">
                                    <div class="card-body">
                                        <div class="d-flex justify-content-between mb-5">
                                            <div>
                                                <span class="d-block font-15 text-dark font-weight-500">Direct Income</span>
                                            </div>

                                        </div>
                                        <div>
                                            <span class="d-block display-4 text-dark mb-5">
                                             <i class="fa fa-inr"></i>

                                                @if ($income !== null)
                                                    {{$income->direct_income}}
                                                @endif


                                            </span>

                                        </div>
                                    </div>
                                </div>




                                <div class="card card-sm">
                                    <div class="card-body">
                                        <div class="d-flex justify-content-between mb-5">
                                            <div>
                                                <span class="d-block font-15 text-dark font-weight-500">Direct Income</span>
                                            </div>

                                        </div>
                                        <div>
                                            <span class="d-block">


                                                @if ($income !== null)
                                                 LF : Rs {{$income->leftincome}}
                                                @endif


                                                <br>

                                                @if ($income !== null)
                                                 RT In: Rs {{$income->rightincome}}
                                                @endif


                                            </span>

                                        </div>
                                    </div>
                                </div>




                                <div class="card card-sm">
                                    <div class="card-body">
                                        <div class="d-flex justify-content-between mb-5">
                                            <div>
                                                <span class="d-block font-15 text-dark font-weight-500">Gen Income</span>
                                            </div>

                                        </div>
                                        <div>
                                            <span class="d-block display-4 text-dark mb-5"><span class="counter-anim">
                                                  <i class="fa fa-inr"></i>

                                                     @if ($income !== null)
                                                    {{$income->generation_income}}
                                                    @endif

                                                </span></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="card card-sm">


                                    <div class="card-body">
                                        <div class="d-flex justify-content-between mb-5">
                                            <div>
                                                <span class="d-block font-15 text-dark font-weight-500">
                                                    Rerutn %
                                                </span>
                                            </div>

                                        </div>
                                        <div>
                                            <span class="d-block display-4 text-dark mb-5">



%

                                            </span>

                                        </div>
                                    </div>
                                </div>

                                <div class="card card-sm">
                                    <div class="card-body">
                                        <div class="d-flex justify-content-between mb-5">
                                            <div>
                                                <span class="d-block font-15 text-dark font-weight-500">Total Income</span>
                                            </div>

                                        </div>
                                        <div>
                                            <span class="d-block display-4 text-dark mb-5">
                                           <i class="fa fa-inr"></i>
                                                 @if ($income !== null)
                                                {{$income->direct_income}}
                                                @endif
                                            </span>

                                        </div>
                                    </div>
                                </div>

                                <div class="card card-sm">
                                    <div class="card-body">
                                        <div class="d-flex justify-content-between mb-5">
                                            <div>
                                                <span class="d-block font-15 text-dark font-weight-500">This Month Income</span>
                                            </div>
                                            <div>
                                                <span class="text-danger font-14 font-weight-500">

                                                     @if ($tmtxi !== 0  && $tmtxi !== 0)

                                                    {{($tmtxi-$lmtxi)/$tmtxi*100}}

                                                    @endif

                                                    %</span>
                                            </div>
                                        </div>
                                        <div>
                                            <span class="d-block display-4 text-dark mb-5">  <i class="fa fa-inr"></i>

                                                 @if ($tmtxi !== null)
                                                {{$tmtxi}}
                                                @endif
                                            </span>

                                        </div>
                                    </div>
                                </div>


                                <div class="card card-sm">
                                    <div class="card-body">
                                        <div class="d-flex justify-content-between mb-5">
                                            <div>
                                                <span class="d-block font-15 text-dark font-weight-500">Withdrawable </span>
                                            </div>
                                            <div>

                                            </div>
                                        </div>
                                        <div>
                                            <span class="d-block display-4 text-dark mb-5" style="color: green!important;">
                                                  <i class="fa fa-inr"></i>

                                             @if ( $income !== null)
                                                    {{$income->withinc}}

                                                     @else

                                                     0

                                                @endif
                                            </span>

                                        </div>
                                    </div>
                                </div>




                            </div>
                        </div>
                    </div>




                    <div class="hk-row">
                        <div class="col-lg-12">


                            <div class="card">




                                <div class="card-body pa-0">


                                    <p style="text-align: center">Income Transactions</p>


                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table class="table table-sm table-hover mb-0">
                                                <thead>
                                                <tr>
                                                    <th class="w-25">Amount</th>
                                                    <th>Direct  Income</th>
                                                    <th>Generation Income</th>
                                                    <th>Pur</th>
                                                    <th>ID</th>
                                                    <th>Generated By</th>
                                                    <th>Generated IC</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($amtx as $ats)

                                                <tr>
                                                    <td>{{$ats->amount}}</td>
                                                    <td>{{$ats->direct_income}}</td>
                                                    <td>{{$ats->generation_income}}</td>
                                                    <td>{{$ats->purpose}}</td>
                                                    <td>{{$ats->created_at}}</td>
                                                    <td>
                                                        {{\App\Model\Customers\Customer::find($ats->payer_id)->fname}}
                                                        {{\App\Model\Customers\Customer::find($ats->payer_id)->lname}}

                                                    </td>
                                                    <td>   MM0000{{\App\Model\Customers\Customer::find($ats->payer_id)->ic_number}}</td>


                                                </tr>

                                                    @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header card-header-action">
                                    <h6>Payout Txts</h6>
                                    <div class="d-flex align-items-center card-action-wrap">
                                        <a href="#" class="inline-block refresh mr-15">
                                            <i class="ion ion-md-arrow-down"></i>
                                        </a>
                                        <a href="#" class="inline-block full-screen mr-15">
                                            <i class="ion ion-md-expand"></i>
                                        </a>
                                        <a class="inline-block card-close" href="#" data-effect="fadeOut">
                                            <i class="ion ion-md-close"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="card-body pa-0">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table class="table table-sm table-hover mb-0">
                                                <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th class="w-40">Amount</th>
                                                    <th class="w-25">Comission</th>
                                                    <th>Tax</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach ($payouts as $pay)



                                                <tr>
                                                    <td>{{$pay->created_at}}</td>
                                                    <td>
                                                        <div class="progress-wrap lb-side-left mnw-125p">
                                                            <div class="progress-lb-wrap">
                                                                <label class="progress-label mnw-50p">

                                                                    <i class="fa fa-inr"></i>

                                                                    {{$pay->amount}}</label>
                                                                <div class="progress progress-bar-rounded progress-bar-xs">
                                                                    <div class="progress-bar bg-primary w-70" role="progressbar"
                                                                         aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>10%</td>
                                                    <td>5%</td>
                                                </tr>

                                                @endforeach



                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->
        </div>




@endsection


@section('footer')









@endsection