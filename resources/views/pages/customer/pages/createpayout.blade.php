@extends('pages.customer.base')

@section('content')
            <!-- Breadcrumb -->
                <nav class="hk-breadcrumb" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-light bg-transparent">
                        <li class="breadcrumb-item"><a href="#">Customer</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create Payout</li>
                    </ol>
                </nav>
                <!-- /Breadcrumb -->

                <!-- Container -->
                <div class="container">
                    <!-- Title -->
                    <div class="hk-pg-header">
                        <h4 class="hk-pg-title">
                            <span class="pg-title-icon"><span class="feather-icon"><i data-feather="external-link"></i></span></span>
                            Create Payout</h4>
                    </div>
                    <!-- /Title -->

                    <!-- Row -->
                    <div class="row">
                        <div class="col-xl-12">
                            <section class="hk-sec-wrapper">
                                <h5 class="hk-sec-title">Create Payout</h5>
                                <div class="row">
                                    <div class="col-sm">
                                        <form class="needs-validation" method="post"  action="{{route('admin.payout.save.two')}}">


                                           <span style="color: red">Withdrawable Amount

                                            @if ($payout !== null && $payout->withinc !== null){

                                            {{$payout->withinc}}

                                                @else

                                                0

                                            @endif
</span>

                                            @csrf
                                            <div class="form-row">
                                                <div class="col-md-3 mb-10">
                                                    <label for="validationCustom01"></label>
                                                    <input type="text" class="form-control"
                                                           readonly
                                                           value="MM0000{{\Illuminate\Support\Facades\Auth::user()->ic_number}}"
                                                           placeholder="IC Number" >

                                                </div>

                                                <input type="hidden" name="customer_id"
                                                       value="{{\Illuminate\Support\Facades\Auth::user()->id}}"
                                                       id="customer_id" class="customer_id">



                                            </div>


                                            <div class="col-md-3 mb-10">
                                                <label for="validationCustom01"></label>
                                                <input type="text" class="form-control"
                                                       placeholder="Amount" disabled id="amount" name="amount" >

                                            </div>







                                            <button class="btn btn-primary registercustomer" disabled type="submit"> Create Payout</button>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>


            @endsection


            @section('footer')



                <link rel="stylesheet" href="https://service.emporiummarketing.com/plugin/ui-autocomplete/jquery-ui.min.css"/>
                <link rel="stylesheet" href="https://service.emporiummarketing.com/plugin/ui-autocomplete/jquery-ui.theme.min.css"/>
                <script src="https://service.emporiummarketing.com/plugin/ui-autocomplete/jquery-ui.min.js" type="text/javascript"></script>


                <script>
                    $(document).ready(function () {


                        var src = '{{route('admin.customer.create.search.id')}}';
                        $("#sponcerid").autocomplete({
                            source: function (request, response) {
                                $.ajax({
                                    url: src,
                                    dataType: "json",
                                    data: {
                                        term: request.term
                                    },
                                    success: function (data) {
                                        response(data);


                                    }


                                });
                            },
                            minLength: 1,
                            select: function (event, ui) {
                                event.preventDefault();
                                $(this).val(ui.item.fname + ' ' +ui.item.lname);
                                $('.customer_id').val(ui.item.id);
                                //  $('.sponser--id').val(ui.item.sponser_id);

                            }

                        });
                    });
                </script>



            @endsection



