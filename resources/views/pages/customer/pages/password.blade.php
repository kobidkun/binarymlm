@extends('pages.customer.base')

@section('content')
    <div class="container-fluid">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12 pa-0">
                <div class="profile-cover-wrap overlay-wrap">
                    <div class="profile-cover-img" style="background-image:url('{{asset('/img/profile/bba5cf5acb1e03045d81555821b986c7461ca64c.jpg')}}')"></div>
                    <div class="bg-overlay bg-trans-dark-60"></div>
                    <div class="container profile-cover-content py-50">
                        <div class="hk-row">
                            <div class="col-lg-6">
                                <div class="media align-items-center">
                                    <div class="media-img-wrap  d-flex">
                                        <div class="avatar">
                                            @if ($customer->customer_to_profile_pictures !== null )

                                                <img src="{{asset('/storage/'.$customer->customer_to_profile_pictures->path)}}"
                                                     alt="user" class="avatar-img rounded-circle">

                                            @else


                                                <img src="{{asset('dist/img/avatar12.jpg')}}"
                                                     alt="user" class="avatar-img rounded-circle">

                                            @endif
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <div class="text-white text-capitalize display-6 mb-5 font-weight-400">{{$customer->fname}} {{$customer->lname}}</div>
                                        <div class="font-14 text-white"><span class="mr-5">
                                                <span class="font-weight-500 pr-5">Phone</span><span class="mr-5">{{$customer->phone}}</span></span><span> <br>
                                                <span class="font-weight-500 pr-5">Mobile</span><span class="mr-5">{{$customer->mobile}}</span></span><span> <br>

                                                <span class="font-weight-500 pr-5">Email</span> <span>{{$customer->email}}</span>
                                        <br> <span class="font-weight-500 pr-5">Hand</span> <span>{{$customer->hand}}</span></span></div>
                                        <br>

                                        <br>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade show active" role="tabpanel">

                        <div class="row">


                            <div class="col-xl-12">






                                <section class="hk-sec-wrapper">


                                    <!--begin: Search Form -->



                                    <div class="row">
                                        <div class="col-sm">
                                            <form class="needs-validation" method="post"  action="{{route('customer.password.edit.save')}}">


                                                @csrf
                                                <div class="form-row">
                                                    <div class="col-md-3 mb-10">
                                                        <label for="validationCustom01">Password</label>
                                                        <input type="password" class="form-control"
                                                               placeholder="Password"  name="password" required>

                                                    </div>

                                                    <div class="col-md-3 mb-10">
                                                        <label for="validationCustom01">Retype Password</label>
                                                        <input type="password" class="form-control"
                                                               placeholder="Password"  name="passwordt" required>

                                                    </div>
                                                </div>

                                                <button class="btn btn-primary registercustomer"  type="submit">Update Password</button>

                                            </form>



                                        </div>
                                    </div>



                                </section>






                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>


@endsection


@section('footer')








@endsection