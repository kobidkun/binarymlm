@extends('pages.customer.base')

@section('content')
    <div class="container-fluid">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12 pa-0">
                <div class="profile-cover-wrap overlay-wrap">
                    <div class="profile-cover-img" style="background-image:url('{{asset('/img/profile/bba5cf5acb1e03045d81555821b986c7461ca64c.jpg')}}')"></div>
                    <div class="bg-overlay bg-trans-dark-60"></div>
                    <div class="container profile-cover-content py-50">
                        <div class="hk-row">
                            <div class="col-lg-6">
                                <div class="media align-items-center">
                                    <div class="media-img-wrap  d-flex">
                                        <div class="avatar">
                                            @if ($customer->customer_to_profile_pictures !== null )

                                                <img src="{{asset('/storage/'.$customer->customer_to_profile_pictures->path)}}"
                                                     alt="user" class="avatar-img rounded-circle">

                                            @else


                                                <img src="{{asset('dist/img/avatar12.jpg')}}"
                                                     alt="user" class="avatar-img rounded-circle">

                                            @endif
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <div class="text-white text-capitalize display-6 mb-5 font-weight-400">{{$customer->fname}} {{$customer->lname}}</div>
                                        <div class="font-14 text-white"><span class="mr-5">
                                                <span class="font-weight-500 pr-5">Phone</span><span class="mr-5">{{$customer->phone}}</span></span><span> <br>
                                                <span class="font-weight-500 pr-5">Mobile</span><span class="mr-5">{{$customer->mobile}}</span></span><span> <br>

                                                <span class="font-weight-500 pr-5">Email</span> <span>{{$customer->email}}</span>
                                        <br> <span class="font-weight-500 pr-5">Hand</span> <span>{{$customer->hand}}</span></span></div>
                                        <br>

                                        <br>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade show active" role="tabpanel">

                        <div class="row">


                            <div class="col-xl-12">





                                <section class="hk-sec-wrapper" style="margin: 15px">


                                    <!--begin: Search Form -->



                                    <div class="row">
                                        <div class="col-sm">
                                            <form  method="post"
                                                   action="{{route('customer.profile.edit.save')}}">


                                                @csrf
                                                <div class="form-row">
                                                    <div class="col-md-3 mb-10">
                                                        <label for="validationCustom01">First name</label>
                                                        <input type="text" class="form-control"
                                                               placeholder="First name"  name="fname"
                                                               value="{{$customer->fname}}"
                                                               readonly
                                                               required>

                                                    </div>
                                                    <div class="col-md-3 mb-10">
                                                        <label for="validationCustom02">Last name</label>
                                                        <input type="text" class="form-control"
                                                               placeholder="Last Name"
                                                               name="lname"
                                                               value="{{$customer->lname}}"

                                                               required>


                                                    </div>



                                                    <div class="col-md-3 mb-10">
                                                        <label for="validationCustom03">Email</label>
                                                        <input
                                                                type="text"
                                                                class="form-control"
                                                                placeholder="Email"
                                                                name="email"
                                                                autocomplete="off"
                                                                required  readonly
                                                                value="{{$customer->email}}"
                                                        >



                                                    </div>




                                                    <div class="col-md-3 mb-10">
                                                        <label for="validationCustom03">mobile</label>
                                                        <input
                                                                type="tel"
                                                                class="form-control"
                                                                placeholder="Mobile"
                                                                name="mobile"  readonly
                                                                autocomplete="off"
                                                                value="{{$customer->mobile}}"

                                                        >



                                                    </div>






                                                    <div class="col-md-3 mb-10">
                                                        <label for="validationCustom03">Gurdian's Name</label>
                                                        <input
                                                                type="text"
                                                                class="form-control"
                                                                placeholder="Gurdian's Name"
                                                                name="g_name"
                                                                autocomplete="off"
                                                                value="{{$customer->g_name}}"

                                                        >



                                                    </div>



                                                    <div class="col-md-3 mb-10">
                                                        <label for="validationCustom03">Date of Birth</label>
                                                        <input
                                                                type="date"
                                                                class="form-control"
                                                                placeholder="Date of Birth"
                                                                id="m_datepicker_1"
                                                                name="dob"
                                                                autocomplete="off"
                                                                value="{{$customer->dob}}"

                                                        >



                                                    </div>





                                                    <div class="col-md-3 mb-10">
                                                        <label for="validationCustom03">Pan Number</label>
                                                        <input
                                                                type="text"
                                                                class="form-control"
                                                                placeholder="Pan Number"
                                                                name="pan"
                                                                autocomplete="off"
                                                                value="{{$customer->pan}}"

                                                        >



                                                    </div>

                                                    <div class="col-md-3 mb-10">
                                                        <label for="validationCustom03">Aadhar Number</label>
                                                        <input
                                                                data-mask="9999 9999 9999 9999"
                                                                class="form-control"
                                                                type="text"
                                                                placeholder="Aadhar Number"
                                                                name="aadhar"
                                                                autocomplete="off"
                                                                value="{{$customer->aadhar}}"

                                                        >



                                                    </div>






                                                </div>

                                                <div class="form-row">








                                                    <div class="col-md-6 mb-10">
                                                        <label for="validationCustom03">Bank Name</label>
                                                        <input
                                                                type="text"
                                                                class="form-control"
                                                                placeholder="Bank Name"
                                                                name="bank_name"
                                                                value="{{$customer->bank_name}}"

                                                        >

                                                    </div>

                                                    <div class="col-md-6 mb-10">
                                                        <label for="validationCustom03">Bank Branch Name</label>
                                                        <input
                                                                type="text"
                                                                class="form-control"
                                                                placeholder="Bank Branch Name"
                                                                name="bank_branch"
                                                                value="{{$customer->bank_branch}}"

                                                        >

                                                    </div>

                                                    <div class="col-md-6 mb-10">
                                                        <label for="validationCustom03">IFSC Code</label>
                                                        <input
                                                                type="text"
                                                                class="form-control"
                                                                placeholder="IFSC Code"
                                                                name="bank_ifsc"
                                                                value="{{$customer->bank_ifsc}}"

                                                        >

                                                    </div>

                                                    <div class="col-md-6 mb-10">
                                                        <label for="validationCustom03">Account Holder Name</label>
                                                        <input
                                                                type="text"
                                                                class="form-control"
                                                                placeholder="Account Holder Name"
                                                                name="bank_account_holder_name"
                                                                value="{{$customer->bank_account_holder_name}}"

                                                        >

                                                    </div>

                                                    <div class="col-md-6 mb-10">
                                                        <label for="validationCustom03">Account Number</label>
                                                        <input
                                                                type="text"
                                                                class="form-control"
                                                                placeholder="Account Number"
                                                                name="bank_account_number"
                                                                value="{{$customer->bank_account_number}}"

                                                        >

                                                    </div>









                                                </div>

                                                <hr>
                                                <div class="form-row">










                                                </div>





                                                <button class="btn btn-primary registercustomer"  type="submit">Update Customer</button>
                                            </form>
                                        </div>
                                    </div>







                                </section>



                                <section class="hk-sec-wrapper "  style="margin: 15px">




                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                @endif


                                <!--begin: Search Form -->



                                    <div class="row" >
                                        <div class="col-sm">
                                            <form
                                                    enctype="multipart/form-data"
                                                    class="needs-validation" method="post"
                                                    action="{{route('admin.customer.profile.img.update')}}">


                                                @csrf
                                                <div class="form-row">
                                                    <div class="col-md-12 mb-10">
                                                        <label for="validationCustom01">Update Profile 500px x 500px</label>
                                                        <input type="file" class="form-control"
                                                               placeholder="Image"  name="img" required>

                                                        <input type="hidden" class="form-control"
                                                               placeholder="Image" value="{{$customer->id}}" name="customer_id" required>


                                                    </div>
                                                </div>


                                                <button class="btn btn-primary "  type="submit">Update Profile</button>

                                            </form>



                                        </div>


                                        <div class="col-sm">
                                            <form
                                                    enctype="multipart/form-data"
                                                    class="needs-validation" method="post"
                                                    action="{{route('customer.profile.aadhar.update')}}">


                                                @csrf
                                                <div class="form-row">
                                                    <div class="col-md-12 mb-10">
                                                        <label for="validationCustom01">Update AAdhar</label>
                                                        <input type="file" class="form-control"
                                                               placeholder="Image"  name="img" required>

                                                        <input type="hidden" class="form-control"
                                                               placeholder="Image" value="{{$customer->id}}" name="customer_id" required>


                                                    </div>
                                                </div>


                                                <button class="btn btn-primary "  type="submit">Update AAdhar</button>

                                            </form>



                                        </div>


                                        <div class="col-sm">
                                            <form
                                                    enctype="multipart/form-data"
                                                    class="needs-validation" method="post"
                                                    action="{{route('customer.profile.pan.update')}}">


                                                @csrf
                                                <div class="form-row">
                                                    <div class="col-md-12 mb-10">
                                                        <label for="validationCustom01">Update PAN</label>
                                                        <input type="file" class="form-control"
                                                               placeholder="Image"  name="img" required>

                                                        <input type="hidden" class="form-control"
                                                               placeholder="Image" value="{{$customer->id}}" name="customer_id" required>


                                                    </div>
                                                </div>


                                                <button class="btn btn-primary "  type="submit">Update PAN</button>

                                            </form>



                                        </div>



                                    </div>









                                </section>




                                <section class="hk-sec-wrapper "  style="margin: 15px">

                                    <div class="row">


                                        @foreach($customer->customer_to_files as $files)




                                            <div class="com-sm">

                                                <a data-fancybox="gallery"
                                                   href="{{asset('/storage'.$files->path)}}">
                                                    <img width="350px" src="{{asset('/storage'.$files->path)}}">
                                                    <h2>{{$files->file_name}}</h2>
                                                </a>

                                            </div>

                                        @endforeach


                                    </div>

                                </section>


                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>


@endsection


@section('footer')



    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>





@endsection