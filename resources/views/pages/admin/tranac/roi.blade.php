@extends('base')

        @section('content')
            <!-- Breadcrumb -->
                <nav class="hk-breadcrumb" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-light bg-transparent">
                        <li class="breadcrumb-item"><a href="#">Admin</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Monthly Roi</li>
                    </ol>
                </nav>
                <!-- /Breadcrumb -->

                <!-- Container -->
                <div class="container">
                    <!-- Title -->
                    <div class="hk-pg-header">
                        <h4 class="hk-pg-title">
                            <span class="pg-title-icon"><span class="feather-icon"><i data-feather="external-link"></i></span></span>
                            Monthly Roi </h4>
                    </div>
                    <!-- /Title -->

                    <!-- Row -->
                    <div class="row">


                        <div class="col-xl-6">
                            <section class="hk-sec-wrapper">
                                <h5 class="hk-sec-title">Monthly Roi</h5>
                                <div class="row">
                                    <div class="col-sm">
                                        <form class="needs-validation" method="post"  action="{{route('admin.roi.save')}}">



                                                <p style="color: red"> Next Update: {{\Carbon\Carbon::now()->endOfMonth()}}
                                                </p>





                                            <p style="color: red"> Current Date: {{\Carbon\Carbon::now()}}</p>



                                            @csrf

                                            @if (Session::has('message'))
                                                <div class="alert alert-danger">{{ Session::get('message') }}</div>
                                            @endif









                                            <button class="btn btn-primary registercustomer"  type="submit"> Perform Roi</button>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>



                        <div class="col-xl-6">
                            <section class="hk-sec-wrapper">
                                <h5 class="hk-sec-title">Binary</h5>
                                <div class="row">
                                    <div class="col-sm">
                                        <form class="needs-validation" method="post"  action="{{route('admin.binary.save')}}">

                                            @if (Session::has('message2'))
                                                <div class="alert alert-danger">{{ Session::get('message2') }}</div>
                                            @endif


                                                <p style="color: red"> Next Update: {{\Carbon\Carbon::now()->endOfMonth()}}
                                                </p>





                                            <p style="color: red"> Current Date: {{\Carbon\Carbon::now()}}</p>



                                            @csrf










                                            <button class="btn btn-primary registercustomer"  type="submit"> Perform Binary</button>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>








                    </div>
                </div>


            @endsection


            @section('footer')






            @endsection



