@extends('base')

@section('content')
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active" aria-current="page">All Customer</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">
        <!-- Title -->

        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">

                   <div class="row">


                       <form class="needs-validation" method="post"  action="{{route('admin.customer.temp.edit.update')}}">


                           @csrf
                           <div class="form-row">


                               <div class="form-row">
                                   <div class="form-group col-md-6">
                                       <label for="inputEmail4">First Name</label>
                                       <input type="text" class="form-control" value="{{$cust->fname}}" name="fname" placeholder="First Name">
                                   </div>
                                   <div class="form-group col-md-6">
                                       <label for="inputPassword4">Last Name</label>
                                       <input type="text" value="{{$cust->lname}}" class="form-control" name="lname" placeholder="Last Name">
                                   </div>
                               </div>


                               <div class="form-row">
                                   <div class="form-group col-md-4">
                                       <label for="inputEmail4">Email</label>
                                       <input type="email" value="{{$cust->email}}" class="form-control" name="email" placeholder="Email">
                                       <input type="hidden" value="{{$cust->id}}"  name="customer_id">
                                   </div>


                                   <div class="form-group col-md-4">
                                       <label for="inputPassword4">Mobile</label>
                                       <input type="text" value="{{$cust->mobile}}"  class="form-control" name="mobile" placeholder="Mobile">
                                   </div>

                                   <div class="form-group col-md-4">
                                       <label for="inputPassword4">Sponcer ID</label>
                                       <input type="text"  value="{{$cust->sponser_id}}" class="form-control" name="sponcer_id" id="sponcerid" placeholder="MM125699">
                                   </div>


                                   <div class="form-group col-md-4">
                                       <label for="inputPassword4">Plan</label>
                                       <input type="text"  value="" class="form-control" name="plan" >
                                   </div>
                               </div>








                           </div>



                           <hr>





                           <div class="form-group">
                               <div class="form-check custom-control custom-checkbox">
                                   <input type="checkbox" class="form-check-input custom-control-input" id="invalidCheck" required>
                                   <label class="form-check-label custom-control-label" for="invalidCheck">
                                       Agree to terms and conditions
                                   </label>
                                   <div class="invalid-feedback">
                                       You must agree before submitting.
                                   </div>
                               </div>
                           </div>


                           <button class="btn btn-primary registercustomer"  type="submit">Update</button>
                       </form>



                   </div>
                </section>
            </div>
        </div>
    </div>


@endsection


@section('footer')


    <link rel="stylesheet" type="text/css" href="{{asset('https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css')}}"/>

    <script type="text/javascript" src="{{asset('/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/vendors/datatables.net-responsive/js/dataTables.responsive.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

    <script>
        $(function() {
            $('#user-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('admin.customer.temp.dt')}}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'fname', name: 'fname' },
                    { data: 'lname', name: 'lname' },
                    { data: 'email', name: 'email' },
                    { data: 'mobile', name: 'mobile' },
                    { data: 'sponser_id', name: 'sponser_id' },
                    { data: 'hand', name: 'hand' },
                    { data: 'is_mobile_verified', name: 'is_mobile_verified' },



                    {data: 'action', name: 'action', orderable: false, searchable: false}

                ]
            });

        });
    </script>





    @endsection