@extends('base')

@section('content')
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active" aria-current="page">Create Epin</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">
        <!-- Title -->
        <div class="hk-pg-header">
            <h4 class="hk-pg-title">
                <span class="pg-title-icon"><span class="feather-icon"><i data-feather="external-link"></i></span></span>
                Create Epin</h4>
        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title">Create Epin</h5>
                    <div class="row">
                        <div class="col-sm">
                            <form class="needs-validation" method="post"  action="{{route('admin.epin.create.mul.post')}}">


                                @csrf
                                <div class="form-row">
                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom01"></label>
                                        <input type="text" class="form-control"
                                               placeholder="Linked Customer" id="sponcerid" name="customer_id" >

                                    </div>




                                </div> <div class="form-row">


                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom01"></label>
                                        <input type="number" class="form-control"
                                               placeholder="No of Epin"  name="no" >

                                    </div>




                                </div>


                                <div class="form-row">
                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom01">Value</label>
                                        <select name="value" class="form-control" id="">
                                            <option value="6500"> 6500</option>
                                            <option value="12900"> 12900</option>
                                            <option value="25800"> 25800</option>
                                            <option value="51600"> 51600</option>

                                            <option value="103200"> 103200</option>
                                        </select>


                                    </div>




                                </div>







                                <button class="btn btn-primary registercustomer"  type="submit">Add Epin</button>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>


@endsection


@section('footer')



    <link rel="stylesheet" href="https://service.emporiummarketing.com/plugin/ui-autocomplete/jquery-ui.min.css"/>
    <link rel="stylesheet" href="https://service.emporiummarketing.com/plugin/ui-autocomplete/jquery-ui.theme.min.css"/>
    <script src="https://service.emporiummarketing.com/plugin/ui-autocomplete/jquery-ui.min.js" type="text/javascript"></script>


    <script>
        $(document).ready(function () {


            var src = '{{route('admin.customer.create.search.id')}}';
            $("#sponcerid").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: src,
                        dataType: "json",
                        data: {
                            term: request.term
                        },
                        success: function (data) {
                            response(data);

                        }


                    });
                },
                minLength: 1,
                select: function (event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.ic_number);
                    //  $('.sponser--id').val(ui.item.sponser_id);

                    if(ui.item.id >= 1){

                        $(".registercustomer").removeAttr("disabled");

                    } else {
                        alert('Sponcer id not Available')
                    }










                }

            });
        });
    </script>



@endsection