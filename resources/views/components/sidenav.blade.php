<!-- Vertical Nav -->
<nav class="hk-nav hk-nav-light">
    <a href="javascript:void(0);" id="hk_nav_close" class="hk-nav-close"><span class="feather-icon"><i data-feather="x"></i></span></a>
    <div class="nicescroll-bar">
        <div class="navbar-nav-wrap">
            <ul class="navbar-nav flex-column">

                @auth('admin')

                    <li class="nav-item">
                        <a class="nav-link link-with-badge" href="javascript:void(0);" data-toggle="collapse" data-target="#app_drp">
                            <span class="feather-icon"><i data-feather="user"></i></span>
                            <span class="nav-link-text">Customer</span>

                        </a>
                        <ul id="app_drp" class="nav flex-column collapse collapse-level-1">
                            <li class="nav-item">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('admin.customer.create')}}">Create Customer</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('admin.customer.all')}}">All Customer</a>
                                    </li>

                                </ul>
                            </li>
                        </ul>
                    </li>



                    <li class="nav-item">
                        <a class="nav-link link-with-badge" href="javascript:void(0);"

                           data-toggle="collapse" data-target="#app_tree">
                            <span class="feather-icon"><i data-feather="command"></i></span>
                            <span class="nav-link-text">Tree</span>

                        </a>
                        <ul id="app_tree" class="nav flex-column collapse collapse-level-1">
                            <li class="nav-item">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('admin.tree.view')}}">Tree View</a>
                                    </li>


                                </ul>
                            </li>
                        </ul>
                    </li>



                    <li class="nav-item">
                        <a class="nav-link link-with-badge" href="javascript:void(0);"

                           data-toggle="collapse" data-target="#app_pin">
                            <span class="feather-icon"><i data-feather="dollar-sign"></i></span>
                            <span class="nav-link-text">EPIN</span>

                        </a>
                        <ul id="app_pin" class="nav flex-column collapse collapse-level-1">
                            <li class="nav-item">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('admin.epin.create.single')}}">Generate Single E-PIN</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('admin.epin.create.mul')}}">Generate Multiple E-PIN</a>
                                    </li>


                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('admin.epin.all')}}"> All E-PIN</a>
                                    </li>


                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('admin.epin.all.used')}}"> All Used E-PIN</a>
                                    </li>


                                </ul>
                            </li>
                        </ul>
                    </li>



                    <li class="nav-item">
                        <a class="nav-link link-with-badge" href="javascript:void(0);"

                           data-toggle="collapse" data-target="#app_txt">
                            <span class="feather-icon"><i data-feather="dollar-sign"></i></span>
                            <span class="nav-link-text">Transactions</span>

                        </a>
                        <ul id="app_txt" class="nav flex-column collapse collapse-level-1">
                            <li class="nav-item">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('admin.txt.view')}}">All Transactions</a>
                                    </li>




                                </ul>
                            </li>
                        </ul>
                    </li>


                    <li class="nav-item">
                        <a class="nav-link link-with-badge" href="javascript:void(0);"

                           data-toggle="collapse" data-target="#app_txp">
                            <span class="feather-icon"><i data-feather="dollar-sign"></i></span>
                            <span class="nav-link-text">Payout</span>

                        </a>
                        <ul id="app_txp" class="nav flex-column collapse collapse-level-1">
                            <li class="nav-item">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('admin.payout.all')}}">All Payout</a>
                                    </li>


  <li class="nav-item">
                                        <a class="nav-link" href="{{route('admin.payout.view')}}">Create Payout</a>
                                    </li>




                                </ul>
                            </li>
                        </ul>
                    </li>



                    <li class="nav-item">
                        <a class="nav-link link-with-badge" href="javascript:void(0);"

                           data-toggle="collapse" data-target="#app_txpr">
                            <span class="feather-icon"><i data-feather="dollar-sign"></i></span>
                            <span class="nav-link-text">Roi</span>

                        </a>
                        <ul id="app_txpr" class="nav flex-column collapse collapse-level-1">
                            <li class="nav-item">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">All Roi</a>
                                    </li>


  <li class="nav-item">
                                        <a class="nav-link" href="{{route('admin.roi.view')}}">Create Roi</a>
                                    </li>




                                </ul>
                            </li>
                        </ul>
                    </li>








                @endauth

                    @auth('customer')


                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('customer.dashboard')}}" data-target="#dash_drp">
                                <span class="feather-icon"><i data-feather="activity"></i></span>
                                <span class="nav-link-text">Dashboard</span>
                            </a>



                        </li>


                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('customer.tree.view')}}" data-target="#dash_drp">
                                <span class="feather-icon"><i data-feather="user"></i></span>
                                <span class="nav-link-text">Tree View</span>
                            </a>



                        </li>


                        <li class="nav-item">
                            <a class="nav-link link-with-badge" href="javascript:void(0);"

                               data-toggle="collapse" data-target="#app_temp">
                                <span class="feather-icon"><i data-feather="users"></i></span>
                                <span class="nav-link-text">Profile</span>

                            </a>
                            <ul id="app_temp" class="nav flex-column collapse collapse-level-1">
                                <li class="nav-item">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{route('customer.profile.edit')}}">Edit</a>
                                        </li>


                                    </ul>
                                </li>
                            </ul>
                        </li>


                        <li class="nav-item">
                            <a class="nav-link link-with-badge" href="javascript:void(0);"

                               data-toggle="collapse" data-target="#app_tempp">
                                <span class="feather-icon"><i data-feather="users"></i></span>
                                <span class="nav-link-text">Payout</span>

                            </a>
                            <ul id="app_tempp" class="nav flex-column collapse collapse-level-1">
                                <li class="nav-item">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{route('customer.payout.create')}}">Create request</a>
                                        </li>


                                    </ul>
                                </li>
                            </ul>
                        </li>
                @endauth

                @guest

                @endguest





              {{--  <li class="nav-item">
                    <a class="nav-link link-with-badge" href="javascript:void(0);" data-toggle="collapse" data-target="#app_drp">
                        <span class="feather-icon"><i data-feather="package"></i></span>
                        <span class="nav-link-text">Application</span>
                        <span class="badge badge-primary badge-pill">4</span>
                    </a>
                    <ul id="app_drp" class="nav flex-column collapse collapse-level-1">
                        <li class="nav-item">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="chats.html">Chat</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="calendar.html">Calendar</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="email.html">Email</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="file-manager.html">File Manager</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#auth_drp">
                        <span class="feather-icon"><i data-feather="zap"></i></span>
                        <span class="nav-link-text">Authentication</span>
                    </a>
                    <ul id="auth_drp" class="nav flex-column collapse collapse-level-1">
                        <li class="nav-item">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#signup_drp">
                                        Sign Up
                                    </a>
                                    <ul id="signup_drp" class="nav flex-column collapse collapse-level-2">
                                        <li class="nav-item">
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="signup.html">Cover</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="signup-simple.html">Simple</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#recover_drp">
                                        Recover Password
                                    </a>
                                    <ul id="recover_drp" class="nav flex-column collapse collapse-level-2">
                                        <li class="nav-item">
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="forgot-password.html">Forgot Password</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="reset-password.html">Reset Password</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="lock-screen.html">Lock Screen</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="404.html">Error 404</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="maintenance.html">Maintenance</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>--}}

            </ul>
           {{-- <hr class="nav-separator">
            <div class="nav-header">
                <span>User Interface</span>
                <span>UI</span>
            </div>
            <ul class="navbar-nav flex-column">



                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#forms_drp">
                        <span class="feather-icon"><i data-feather="server"></i></span>
                        <span class="nav-link-text">Forms</span>
                    </a>
                    <ul id="forms_drp" class="nav flex-column collapse collapse-level-1">
                        <li class="nav-item">
                            <ul class="nav flex-column">

                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('frontend.forms')}}">Form Layout</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('frontend.form')}}">Form Validation</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('frontend.form_upload')}}">File Upload</a>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#tables_drp">
                        <span class="feather-icon"><i data-feather="list"></i></span>
                        <span class="nav-link-text">Tables</span>
                    </a>
                    <ul id="tables_drp" class="nav flex-column collapse collapse-level-1">
                        <li class="nav-item">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('frontend.basictable')}}">Basic Table</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('frontend.datatable')}}">Data Table</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('frontend.responsivetable')}}">Responsive Table</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('frontend.editabletable')}}">Editable Table</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>


            </ul>
            <hr class="nav-separator">
            <div class="nav-header">
                <span>Getting Started</span>
                <span>GS</span>
            </div>
            <ul class="navbar-nav flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="documentation.html" >
                        <span class="feather-icon"><i data-feather="book"></i></span>
                        <span class="nav-link-text">Documentation</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link link-with-badge" href="#">
                        <span class="feather-icon"><i data-feather="eye"></i></span>
                        <span class="nav-link-text">Changelog</span>
                        <span class="badge badge-sm badge-danger badge-pill">v 1.0</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <span class="feather-icon"><i data-feather="headphones"></i></span>
                        <span class="nav-link-text">Support</span>
                    </a>
                </li>




            </ul>--}}




        </div>
    </div>
</nav>
<div id="hk_nav_backdrop" class="hk-nav-backdrop"></div>
<!-- /Vertical Nav -->