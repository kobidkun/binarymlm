<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_to_customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('customer_id')->nullable();
            $table->text('ref_id')->nullable();
            $table->text('ic_number')->nullable();
            $table->text('customer_parient_ic_number')->nullable();
            $table->text('up_line_customer_id')->nullable();
            $table->text('left')->nullable();
            $table->text('right')->nullable();
            $table->string('left_id')->nullable();
            $table->string('right_id')->nullable();
            $table->string('left_ic_number')->nullable();
            $table->string('right_ic_number')->nullable();
            $table->string('hand')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_to_customers');
    }
}
