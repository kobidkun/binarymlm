<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerToEarningToTranactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_to_earning_to_tranactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('customer_id');
            $table->text('amount')->nullable();
            $table->text('direct_income')->nullable();
            $table->text('direct_income_payable')->nullable();
            $table->text('generation_income')->nullable();
            $table->text('generation_income_payable')->nullable();
            $table->text('purpose')->nullable();
            $table->text('payer_id')->nullable();
            $table->text('oth1')->nullable();
            $table->text('oth2')->nullable();
            $table->text('oth3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_to_earning_to_tranactions');
    }
}
