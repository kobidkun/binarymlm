<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


Auth::routes();




/////admin login
///
///
///

//////front
///
///



Route::get('/', 'Front\PageController@HomePage')->name('front.home');
Route::get('/about', 'Front\PageController@About')->name('front.about');
Route::get('/returns', 'Front\PageController@Returns')->name('front.returns');
Route::get('/sign-up', 'Front\PageController@SignUp')->name('front.signup');
Route::post('/sign-up', 'Customer\Auth\LoginController@RegisterTemp')->name('front.signup.post');
Route::get('/otp/{id}', 'Front\PageController@OTP')->name('front.otp');
Route::post('/otp/verify', 'Front\PageController@OTPVerify')->name('front.otp.verify');
Route::get('/success', 'Front\PageController@Success')->name('front.success');
Route::get('/verify', 'Front\PageController@GetAllCustomerviaSearch')->name('front.api.search');
Route::post('/register/customer', 'Front\PageController@CreateCustomerfront')->name('front.register.customer');






//////////////////Ebd home ###########

Route::get('admin/login', 'Admin\Auth\LoginController@loginview')
    ->name('admin.login.view');

Route::get('admin/logout', 'Admin\Auth\LoginController@logout')
    ->name('admin.logout');


Route::post('admin/login', 'Admin\Auth\LoginController@StoreLogin')
    ->name('admin.login.post');









//customer

Route::get('/admin/customer/create', 'Admin\Customer\MamnageCustomer@CreateCustomer')
    ->name('admin.customer.create');


Route::get('/admin/customer', 'Admin\Customer\MamnageCustomer@AllCustomer')
    ->name('admin.customer.all');

Route::get('/admin/customer/api-v2', 'Admin\Customer\MamnageCustomer@AllCustomerapiyttyygh')
    ->name('admin.customer.all.apiv2');

Route::get('/admin/customer/{id}', 'Admin\Customer\MamnageCustomer@Details')
    ->name('admin.customer.details');



Route::get('/admin/customer/password/view/{id}', 'Admin\Customer\MamnageCustomer@Password')
    ->name('admin.customer.password');

Route::post('/admin/customer/password/update/{id}', 'Admin\Customer\MamnageCustomer@PasswordUpdate')
    ->name('admin.customer.password.update');



Route::get('/admin/customer/profile/{id}', 'Admin\Customer\MamnageCustomer@Profile')
    ->name('admin.customer.profile');

Route::post('/admin/customer/profile/{id}', 'Admin\Customer\MamnageCustomer@ProfileUpdate')
    ->name('admin.customer.profile.update');

Route::post('/admin/customer/profile/img/upload', 'Admin\Customer\MamnageCustomer@ProfileImgUpdateupload')
    ->name('admin.customer.profile.img.update');

Route::post('/admin/update-plan', 'Admin\Customer\MamnageCustomer@UpdatePlantwo')
    ->name('admin.customer.update.plan');



Route::get('/admin/customer/files/{id}', 'Admin\Customer\MamnageCustomer@Files')
    ->name('admin.customer.files');

Route::post('/admin/customer/files/{id}', 'Admin\Customer\MamnageCustomer@FilesUpdate')
    ->name('admin.customer.files.files.update');



Route::post('/admin/customer/create', 'Admin\Customer\MamnageCustomer@CreateCustomerSave')
    ->name('admin.customer.create.save');


Route::get('/admin/customer/search', 'Admin\Customer\MamnageCustomer@GetAllCustomerviaSearch')
    ->name('admin.customer.create.search');

Route::get('/search/byid', 'Admin\Customer\MamnageCustomer@GetAllCustomerviaSearchid')
    ->name('admin.customer.create.search.id');



//// temp cust
///

Route::get('/admin/temp/customer', 'Admin\Temp\ManageTempCustomer@AllCustomer')
    ->name('admin.customer.temp.all');

Route::get('/admin/temp/customer/api', 'Admin\Temp\ManageTempCustomer@AllCustomerapiyttyygh')
    ->name('admin.customer.temp.dt');

Route::get('/admin/temp/customer/edit/{id}', 'Admin\Temp\ManageTempCustomer@EditCustVoew')
    ->name('admin.customer.temp.edit');

Route::post('/admin/temp/customer/update', 'Admin\Temp\ManageTempCustomer@EditCustVoewupdate')
    ->name('admin.customer.temp.edit.update');

Route::get('/admin/temp/customer/delete/{id}', 'Admin\Temp\ManageTempCustomer@DeleteTemp')
    ->name('admin.customer.temp.convert.delete');






Route::get('/admin/temp/customer/convert/{id}', 'Admin\Customer\MamnageCustomer@ConvertCustomer')
    ->name('admin.customer.temp.convert');



Route::get('/admin/temp/customer/convert/process/{id}', 'Admin\Customer\MamnageCustomer@ConvertCustomerprocess')
    ->name('admin.customer.temp.convert.process');



// tree


Route::get('admin/dashboard/admin/tree-view/by-id/down-line/{id}', 'Admin\Tree\ManageTree@GetAllDownloneByid')
    ->name('admin.tree.view.by.id.downline');

Route::get('admin/tree/down-line/foradmin', 'Admin\Tree\ManageTree@GetAllDownlone')
    ->name('admin.tree.view.by.id.admin');

Route::get('admin/dashboard/tree-view/', 'Admin\Tree\ManageTree@ViewTreeView')
    ->name('admin.tree.view');


Route::get('admin/dashboard/tree-view/view/{id}', 'Admin\Tree\ManageTree@ViewTreeViewbyid')
    ->name('admin.tree.view.by.id');

Route::get('/dashboard/admin/tree-view/view/{id}', 'Admin\Tree\ManageTree@ViewTreeViewbyid');

// epin

Route::get('admin/epin/create', 'Admin\Epin\ManageEpin@CreateSingleview')
    ->name('admin.epin.create.single');

Route::post('admin/epin/create', 'Admin\Epin\ManageEpin@CreateSingle')
    ->name('admin.epin.create.single.post');

Route::get('admin/epin/create/mul', 'Admin\Epin\ManageEpin@CreateMulview')
    ->name('admin.epin.create.mul');


Route::post('admin/epin/create/mul', 'Admin\Epin\ManageEpin@CreateSinglemul')
    ->name('admin.epin.create.mul.post');


Route::get('admin/epin/all/api', 'Admin\Epin\ManageEpin@EpinApi')
    ->name('admin.epin.all.api');


Route::get('admin/epin/all', 'Admin\Epin\ManageEpin@AllEpin')
    ->name('admin.epin.all');


Route::get('admin/epin-used/all/api', 'Admin\Epin\ManageEpin@EpinusedApi')
    ->name('admin.epin.all.api.used');


Route::get('admin/epin-used/all', 'Admin\Epin\ManageEpin@AllEpinused')
    ->name('admin.epin.all.used');

///// txts

Route::get('admin/txts/all', 'Admin\Misc\MiscManage@TranactionsView')
    ->name('admin.txt.view');



Route::get('admin/txts/api/all', 'Admin\Misc\MiscManage@TranactionsViewApi')
    ->name('admin.txt.api');





Route::get('admin/payout/api/all', 'Admin\Misc\MiscManage@PayoutViewApi')
    ->name('admin.payout.api');

Route::get('admin/payout/all', 'Admin\Misc\MiscManage@PayoutView')
    ->name('admin.payout.all');

Route::get('admin/payout/create', 'Admin\Misc\MiscManage@PayoutViewnew')
    ->name('admin.payout.view');

Route::post('admin/payout/create/122', 'Admin\Misc\MiscManage@PayoutViewnewsave')
    ->name('admin.payout.save.two');


Route::get('admin/roi/create', 'Admin\Misc\MiscManage@Roi')
    ->name('admin.roi.view');




Route::post('admin/roi/save', 'Admin\Misc\MiscManage@RoiSave')
    ->name('admin.roi.save');

Route::post('admin/binary/save', 'Admin\Misc\MiscManage@BinarySave')
    ->name('admin.binary.save');







Route::post('admin/payout/create', 'Admin\Misc\MiscManage@Roisave')
    ->name('admin.roi.save');




///////////////////////////////////////////customer Auth /////////////////////////////////////
///


Route::get('customer/login', 'Customer\Auth\LoginController@loginview')
    ->name('customer.login.view');

Route::get('customer/logout', 'Customer\Auth\LoginController@logout')
    ->name('customer.logout');


Route::post('customer/login', 'Customer\Auth\LoginController@StoreLogin')
    ->name('customer.login.post');

// tree customer

Route::get('customer/tree-view', 'Customer\Tree\ManageTreeController@ShowTreeView')
    ->name('customer.tree.view');

Route::get('customer/tree-view/{id}', 'Customer\Tree\ManageTreeController@GetTreeApi')
    ->name('customer.tree.view.id');

Route::get('customer/tree-view-by-id/{id}', 'Customer\Tree\ManageTreeController@ViewTreeViewbyid')
    ->name('customer.tree.view.by.id');

Route::get('customer/dashboard', 'Customer\Profile\ManageProfile@Dashboard')
    ->name('customer.dashboard');


Route::get('customer/myprofile/edit', 'Customer\Profile\ManageProfile@ViewProfile')
    ->name('customer.profile.edit');

Route::post('customer/myprofile/edit', 'Customer\Profile\ManageProfile@SaveProfile')
    ->name('customer.profile.edit.save');

Route::get('customer/password/edit', 'Customer\Profile\ManageProfile@ChangePassword')
    ->name('customer.password.edit');

Route::post('customer/password/edit', 'Customer\Profile\ManageProfile@ChangePasswordSave')
    ->name('customer.password.edit.save');

Route::post('customer/myprofile/image', 'Customer\Profile\ManageProfile@ProfileImgUpdateupload')
    ->name('customer.profile.image.save');


Route::post('customer/aadhar/update', 'Customer\Profile\ManageProfile@UploadAAdhar')
    ->name('customer.profile.aadhar.update');

Route::post('customer/pan/update', 'Customer\Profile\ManageProfile@UploadPan')
    ->name('customer.profile.pan.update');

// epin


Route::get('customer/pin/act', 'Customer\Profile\ManageProfile@ActivatebyEpin')
    ->name('customer.profile.epin');

Route::post('customer/pin/act', 'Customer\Profile\ManageProfile@VerifybyEpin')
    ->name('customer.profile.epin.post');

Route::get('customer/payout', 'Customer\Profile\ManageProfile@Payoutview')
    ->name('customer.payout.create');

Route::post('customer/pin/act', 'Customer\Profile\ManageProfile@VerifybyEpin')
    ->name('customer.profile.epin.post');

