<?php

namespace App\Http\Controllers\Admin\Tree;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Customers\Customer;
use App\Model\Customers\CustomerToCustomer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class ManageTree extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function ViewTreeView(){

        return view('pages.admin.tree.tree');




    }


    public function ViewTreeViewbyid($id){
        $findReqUserid = Customer::findorfail($id);

        return view('pages.admin.tree.treeid')->with([
            'customer' => $findReqUserid
        ]);




    }
    public function GetAllDownloneByid($id){



        $findReqUserid = Customer::findorfail($id);

        //  dd($findReqUserid);



        $findReqUser = Customer::findorfail($findReqUserid->id);

        if ($findReqUserid->left_id != null){
            $leftWing = Customer::findorfail($findReqUserid->left_id);
        } else {
            $leftWing = null;
        }


        if ($findReqUserid->right_id != null){
            $rightWing = Customer::findorfail($findReqUserid->right_id);
        } else {
            $rightWing = null;
        }










        return response()->json(

            ['customer' => $findReqUser,
                'left' =>   $leftWing,
                'right' =>   $rightWing
            ]



        );
















    }



    public function GetAllDownlone(){



        $findReqUserid = Customer::first();

        // dd($findReqUserid);

      //  return $findReqUserid;



       // $findReqUser = Customer::findorfail($findReqUserid->id);

        if ($findReqUserid->left_id != null){
            $leftWing = Customer::findorfail($findReqUserid->left_id);
        } else {
            $leftWing = null;
        }


        if ($findReqUserid->right_id != null){
            $rightWing = Customer::findorfail($findReqUserid->right_id);
        } else {
            $rightWing = null;
        }










        return response()->json(

            ['customer' => $findReqUserid,
                'left' =>   $leftWing,
                'right' =>   $rightWing
            ]



        );





    }
}
