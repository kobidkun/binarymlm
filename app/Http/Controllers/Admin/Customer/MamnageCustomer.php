<?php

namespace App\Http\Controllers\Admin\Customer;

use App\Http\Requests\Request\Admin\CustomerFileUploader;
use App\Http\Requests\Request\Admin\CustomerProfileUploader;
use App\Model\Customers\Customer;
use App\Model\Customers\CustomerToCustomer;
use App\Model\Customers\CustomerToFiles;
use App\Model\Customers\CustomerToProfilePicture;
use App\Model\Temp\CreateCustomer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;


class MamnageCustomer extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin']);
    }


    public function CreateCustomer(){
        return view('pages.admin.customer.createcustomer');
    }


    public function CreateCustomerSave(Request $request ){

        if ($request->sponser_id != null ){
            $getlastrecord = Customer::orderBy('created_at', 'desc')->first();

            $icnum = $getlastrecord->ic_number +1;

        } else {

            $icnum = 1;
        }










        $s = new Customer();
        $s->fname = $request->fname;
        $s->lname = $request->lname;
        $s->ic_number = $icnum ;
        $s->g_name = $request->g_name;
        $s->commission = $request->commission;
        $s->dob = $request->dob;
        $s->sex = $request->sex;
        $s->ref = $request->ref;
        $s->hand = $request->hand;
     //   $s->right_ic_number = $request->right_ic_number;
       // $s->left_ic_number = $request->left_ic_number;
      //  $s->right_id = $request->right_id;
     //   $s->left_id = $request->left_id;
        $s->sponser_id = $request->sponser_id;
        $s->street = $request->street;
        $s->locality = $request->locality;
        $s->city = $request->city;
        $s->state = $request->state;
        $s->country = $request->country;
        $s->pin = $request->pin;
        $s->permanent_address = $request->permanent_address;
        $s->supply_address = $request->supply_address;
        $s->email = $request->email;
        $s->mobile = $request->mobile;
        $s->phone = $request->phone;
        $s->nominee_name = $request->nominee_name;
        $s->nominee_relation = $request->nominee_relation;
        $s->nominee_address = $request->nominee_address;
        $s->nominee_dob = $request->nominee_dob;
        $s->bank_name = $request->bank_name;
        $s->bank_ifsc = $request->bank_ifsc;
        $s->bank_micr = $request->bank_micr;
        $s->bank_account_holder_name = $request->bank_account_holder_name;
        $s->bank_account_number = $request->bank_account_number;
        $s->bank_branch = $request->bank_branch;
        $s->pan = $request->pan;
        $s->aadhar = $request->aadhar;
        $s->gstin = $request->gstin;
        $s->gstin_state = $request->gstin_state;
        $s->billing_name = $request->billing_name;
        $s->password = bcrypt($request->password);
        $s->save();

      //  dd($s->id);


        $custcreateid = $s->id;




        $data = $s;

        $customertocust = new CustomerToCustomer();

        $customertocust->customer_id  = $s->id;
        $customertocust->ic_number  = $s->ic_number;
        $customertocust->customer_parient_ic_number  = $s->sponser_id;


        $customertocust->hand  = $s->hand;

        $customertocust->save();


        if ($request->sponser_id !== null){





            //update customer uplone;

            $findupline = Customer::where('ic_number', $data->sponser_id)->first();

            if ($request->hand === 'right'){

                $findupline->right_id = $s->id;
                $findupline->right_ic_number = $s->ic_number;

                $updatecusttocust = CustomerToCustomer::where('customer_id', $data->id)->first();

                $updatecusttocust->right_ic_number = $s->ic_number;
                $updatecusttocust->right_id = $s->id;

                $updatecusttocust->save();


            } elseif ($request->hand === 'left'){

                $findupline->left_id = $s->id;
                $findupline->left_ic_number = $s->ic_number;


                $updatecusttocust = CustomerToCustomer::where('customer_id', $data->id)->first();


                $updatecusttocust->left_ic_number = $s->ic_number;
                $updatecusttocust->left_id = $s->id;

                $updatecusttocust->save();

            }

            $findupline->save();

            //  if

            //
        }





        return back();
    }


    public function ConvertCustomer($id){
        $a = CreateCustomer::find($id);

        return view('pages.admin.temp.confirm')->with([
            'customer' => $a
        ]);
    }

    public function ConvertCustomerprocess($id ){

        $cust = CreateCustomer::find($id);

        if ($cust->sponser_id != null ){
            $getlastrecord = Customer::orderBy('created_at', 'desc')->first();

            $icnum = $getlastrecord->ic_number +1;

        } else {

            $icnum = 1;
        }










        $s = new Customer();
        $s->fname = $cust->fname;
        $s->lname = $cust->lname;
        $s->ic_number = $icnum ;
        $s->g_name = $cust->g_name;
        $s->commission = $cust->commission;
        $s->dob = $cust->dob;
        $s->sex = $cust->sex;
        $s->ref = $cust->ref;
        $s->hand = $cust->hand;
        $s->plan = $cust->plan;
        //   $s->right_ic_number = $cust->right_ic_number;
        // $s->left_ic_number = $cust->left_ic_number;
        //  $s->right_id = $cust->right_id;
        //   $s->left_id = $cust->left_id;
        $s->sponser_id = $cust->sponser_id;
        $s->street = $cust->street;
        $s->locality = $cust->locality;
        $s->city = $cust->city;
        $s->state = $cust->state;
        $s->country = $cust->country;
        $s->pin = $cust->pin;
        $s->permanent_address = $cust->permanent_address;
        $s->supply_address = $cust->supply_address;
        $s->email = $cust->email;
        $s->mobile = $cust->mobile;
        $s->phone = $cust->phone;
        $s->nominee_name = $cust->nominee_name;
        $s->nominee_relation = $cust->nominee_relation;
        $s->nominee_address = $cust->nominee_address;
        $s->nominee_dob = $cust->nominee_dob;
        $s->bank_name = $cust->bank_name;
        $s->bank_ifsc = $cust->bank_ifsc;
        $s->bank_micr = $cust->bank_micr;
        $s->bank_account_holder_name = $cust->bank_account_holder_name;
        $s->bank_account_number = $cust->bank_account_number;
        $s->bank_branch = $cust->bank_branch;
        $s->pan = $cust->pan;
        $s->aadhar = $cust->aadhar;
        $s->gstin = $cust->gstin;
        $s->gstin_state = $cust->gstin_state;
        $s->billing_name = $cust->billing_name;
        $s->password = bcrypt($cust->password);
        $s->save();

        //  dd($s->id);


        $custcreateid = $s->id;




        $data = $s;

        $customertocust = new CustomerToCustomer();

        $customertocust->customer_id  = $s->id;
        $customertocust->ic_number  = $s->ic_number;
        $customertocust->customer_parient_ic_number  = $s->sponser_id;


        $customertocust->hand  = $s->hand;

        $customertocust->save();


        if ($cust->sponser_id !== null){





            //update customer uplone;

            $findupline = Customer::where('ic_number', $data->sponser_id)->first();

            if ($cust->hand === 'right'){

                $findupline->right_id = $s->id;
                $findupline->right_ic_number = $s->ic_number;

                $updatecusttocust = CustomerToCustomer::where('customer_id', $data->id)->first();

                $updatecusttocust->right_ic_number = $s->ic_number;
                $updatecusttocust->right_id = $s->id;

                $updatecusttocust->save();


            } elseif ($cust->hand === 'left'){

                $findupline->left_id = $s->id;
                $findupline->left_ic_number = $s->ic_number;


                $updatecusttocust = CustomerToCustomer::where('customer_id', $data->id)->first();


                $updatecusttocust->left_ic_number = $s->ic_number;
                $updatecusttocust->left_id = $s->id;

                $updatecusttocust->save();

            }

            $findupline->save();

            //  if

            //
        }
        
        
            ////sms
        ///
        ///
        ///


        $smssmg = 'Hi '.$s->fname.' your payment has been approved and you can login with the your login password which is password Thanks'.env('APP_NAME');

        // send sms

        //Your authentication key
        $authKey = "143565AIE7jf1Mb5abd98a0";

//Multiple mobiles numbers separated by comma
        $mobileNumber = $s->mobile;

//Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "MULTST";

//Your message to send, Add URL encoding here.
        $message = urlencode($smssmg );

//Define route
        $route = "4";
//Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

//API URL
        $url="http://api.msg91.com/api/sendhttp.php";

// init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


//Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


//get response
        $output = curl_exec($ch);

//Print error if any
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);




        //end ssms


        $cust->create_customer_to_verifies->delete();

        $cust->delete();






        return redirect(route('admin.customer.all'));
    }



    public function PostRegisterUpdatemanyuser($s,$request) {

    }

    public function CustomerDetails($id){
        return view('pages.admin.customer.details');
    }


    public function GetAllCustomerviaSearch(Request $request){






        $query = $request->get('term','');

        $customers = Customer::where('fname', 'LIKE', '%' . $query . '%')
            ->orwhere('ic_number', 'LIKE', '%' . $query . '%')

            ->take(5)->get();

        $data=array();
        foreach ($customers as $customer) {
            $data[]=array(
                'value'=>$customer->fname .' '. $customer->lname,
                'lname'=>$customer->lname,
                'id'=>$customer->id,
                'ic_number'=>$customer->ic_number,
                'gstin'=>$customer->gstin,
                'street'=>$customer->street,
                'locality'=>$customer->locality,
                'city'=>$customer->city,
                'state'=>$customer->state,
                'country'=>$customer->country,
                'pin' => $customer->pin,
                'permanent_address' => $customer->permanent_address,
                'email' => $customer->email,
                'mobile' => $customer->mobile,
                'pan' => $customer->pan,
                'gstin_state' => $customer->gstin_state,
                'billing_name' => $customer->billing_name,
                'commission' => $customer->commission,
                'left_id' => $customer->left_id,
                'right_id' => $customer->right_id,
                'right_ic_number' => $customer->right_ic_number,
                'left_ic_number' => $customer->left_ic_number,

            );
        }
        if(count($data))
            return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }




    public function GetAllCustomerviaSearchid(Request $request){






        $query = $request->get('term','');

        $customers = Customer::where('fname', 'LIKE', '%' . $query . '%')
            ->orwhere('ic_number', 'LIKE', '%' . $query . '%')

            ->take(5)->get();

        $data=array();
        foreach ($customers as $customer) {
            $data[]=array(
                'value'=>$customer->fname .' '. $customer->lname.' MM0000'.$customer->ic_number,
                'lname'=>$customer->lname,
                'id'=>$customer->id,
                'ic_number'=>$customer->ic_number,
                'gstin'=>$customer->gstin,
                'street'=>$customer->street,
                'locality'=>$customer->locality,
                'city'=>$customer->city,
                'state'=>$customer->state,
                'country'=>$customer->country,
                'pin' => $customer->pin,
                'permanent_address' => $customer->permanent_address,
                'email' => $customer->email,
                'mobile' => $customer->mobile,
                'pan' => $customer->pan,
                'gstin_state' => $customer->gstin_state,
                'billing_name' => $customer->billing_name,
                'commission' => $customer->commission,
                'left_id' => $customer->left_id,
                'right_id' => $customer->right_id,
                'right_ic_number' => $customer->right_ic_number,
                'left_ic_number' => $customer->left_ic_number,

            );
        }
        if(count($data))
            return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }

    public function AllCustomer(){
        return view('pages.admin.customer.all');
    }

    public function Details($id){


        $customer = Customer::findorfail($id);

       // return $customer;


        return view('pages.admin.customer.details')->with([
            'customer' => $customer
        ]);
    }


    public function Profile($id){


        $customer = Customer::findorfail($id);

       // return $customer;


        return view('pages.admin.customer.profiles.profile')->with([
            'customer' => $customer
        ]);
    }




    public function ProfileUpdate($id, Request $request){


        $s = Customer::findorfail($id);

       // return $customer;


        $s->fname = $request->fname;
        $s->lname = $request->lname;

        $s->g_name = $request->g_name;
        $s->commission = $request->commission;
        $s->dob = $request->dob;
        $s->sex = $request->sex;
        $s->street = $request->street;
        $s->locality = $request->locality;
        $s->city = $request->city;
        $s->state = $request->state;
        $s->country = $request->country;
        $s->pin = $request->pin;
        $s->permanent_address = $request->permanent_address;
        $s->supply_address = $request->supply_address;
        $s->email = $request->email;
        $s->mobile = $request->mobile;
        $s->phone = $request->phone;
        $s->nominee_name = $request->nominee_name;
        $s->nominee_relation = $request->nominee_relation;
        $s->nominee_address = $request->nominee_address;
        $s->nominee_dob = $request->nominee_dob;
        $s->bank_name = $request->bank_name;
        $s->bank_ifsc = $request->bank_ifsc;
        $s->bank_micr = $request->bank_micr;
        $s->bank_account_holder_name = $request->bank_account_holder_name;
        $s->bank_account_number = $request->bank_account_number;
        $s->bank_branch = $request->bank_branch;
        $s->pan = $request->pan;
        $s->aadhar = $request->aadhar;
        $s->gstin = $request->gstin;
        $s->gstin_state = $request->gstin_state;
        $s->billing_name = $request->billing_name;
        $s->save();

        return back();
    }

    public function ProfileImgUpdateupload(CustomerProfileUploader $request){


        if ( $request->img  === null ){
            return 'No  image ';
        }





        // return $customer;

        $imglocname =  time().'-'.$request->img->getClientOriginalName();
        $imglocnamepath = '/images/profile/hd/'.$imglocname;

        $imgloc = $request->img->storeAs('public/images/profile/hd', $imglocname);


        if(@is_array(getimagesize($imgloc))){
            $image = true;
        } else {
            $image = false;
        }





        $img = new CustomerToProfilePicture();

        $img->customer_id = $request->customer_id;
        $img->is_img = $image;
        $img->path = $imglocnamepath;

        $img->save();

        return back();



    }

    public function Password($id ){

        $customer = Customer::findorfail($id);

        // return $customer;


        return view('pages.admin.customer.profiles.password')->with([
            'customer' => $customer
        ]);




    }

    public function PasswordUpdate($id, Request $request){





        $customer = Customer::findorfail($id);

        // return $customer;

        $customer->password = bcrypt($request->password);

        $customer->save();

        return back();
    }


    public function Files($id){


        $customer = Customer::findorfail($id);

       // return $customer;


        return view('pages.admin.customer.profiles.files')->with([
            'customer' => $customer
        ]);
    }


    public function FilesUpdate(CustomerFileUploader  $request){

     if ( $request->img  === null ){
            return 'No  image ';
        }


            $imglocname = time() . '-' . $request->img->getClientOriginalName();
            $imglocnamepath = '/images/files/hd/' . $imglocname;

            $imgloc = $request->img->storeAs('public/images/files/hd', $imglocname);


        if(@is_array(getimagesize($imgloc))){
            $image = true;
        } else {
            $image = false;
        }






            $img = new CustomerToFiles();

            $img->customer_id = $request->customer_id;
            $img->is_img = 'yes';
            $img->file_name = $request->file_name;
            $img->path = $imglocnamepath;

            $img->save();



        return back();




    }

    public function AllCustomerapiyttyygh(){


        {
            $customers = Customer::select([
                'id',
                'fname',
                'lname',
                'email',
                'mobile',
                'ic_number',
                'sponser_id',
                'plan',
                'pan',
                'commission',
            ])->orderBy('updated_at', 'DESC');

            return DataTables::of($customers)
                ->addColumn('action', function ($invoice) {
                    return '<a href="' . route('admin.customer.details', $invoice->id) . '" class=" btn btn-xs btn-primary" title="View details"><i class="la la-edit"></i>View Details</a>';
                })

                ->editColumn('plan', function ($invoice) {

                    if ($invoice->plan === null){

                        return '<button  class=" btn btn-xs btn-danger" title="View details">Not Active</a>';

                    } else{
                        return '<button  class=" btn btn-xs btn-info" title="View details">Active</a>';
                    }


                })

                ->editColumn('ic_number', function ($invoice) {

                    return 'MM0000'.$invoice->ic_number;


                })

                ->editColumn('sponser_id', function ($invoice) {

                    return 'MM0000'.$invoice->sponser_id;


                })

                ->addColumn('planupdate', function ($invoice) {
                    return '<form  method="post" action="'.route('admin.customer.update.plan').'">

                                                        <input type="hidden" value="'.$invoice->id.'" name="customer_id">
                                                        <input type="hidden" value="'.csrf_token().'" name="_token">

                                                      
                                                        
                                                                <label for="validationCustom01">Plan</label>
                                                                <select name="plan" class="form-control" id="">
                                                                    <option value="'.$invoice->plan.'"> '.$invoice->plan.'</option>
                                                                    <option value=""> 0</option>
                                                                    <option value="6500"> 6500</option>
                                                                    <option value="12900"> 12900</option>
                                                                    <option value="25800"> 25800</option>
                                                                    <option value="51600"> 51600</option>

                                                                    <option value="103200"> 103200</option>
                                                                </select>

                                                         

<button class="btn btn-primary"  type="submit">Update Plan</button>
                                                    </form>';
                })
                ->rawColumns(['action','planupdate','plan'])
                ->make(true);
        }

    }


    public function UpdatePlantwo(Request $request){

      //  dd($request->customer_id);

        $a = Customer::findorfail($request->customer_id);

        $a->plan = $request->plan;

        $a->save();

        $smssmg = 'Hi '.$a->fname.' , your payment of Rs. '.$request->plan.'  Has been received for ic no. '.$a->ic_number.' You are now active member Thanks '.env('APP_NAME');

        // send sms

        //Your authentication key
        $authKey = "143565AIE7jf1Mb5abd98a0";

//Multiple mobiles numbers separated by comma
        $mobileNumber = $a->mobile;

//Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "MULTST";

//Your message to send, Add URL encoding here.
        $message = urlencode($smssmg );

//Define route
        $route = "4";
//Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

//API URL
        $url="http://api.msg91.com/api/sendhttp.php";

// init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


//Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


//get response
        $output = curl_exec($ch);

//Print error if any
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);








        return back();


    }



}
