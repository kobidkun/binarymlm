<?php

namespace App\Http\Controllers\Admin\Temp;

use App\Model\Temp\CreateCustomer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;


class ManageTempCustomer extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin']);
    }

    public function AllCustomer(){

        return view('pages.admin.temp.all');

    }


    public function EditCustVoew($id){

        $a = CreateCustomer::find($id);


        return view('pages.admin.temp.edit')->with(['cust' => $a]);
    }


    public function EditCustVoewupdate(Request $request){

        $a = CreateCustomer::find($request->customer_id);

        $a->email = $request->email;
        $a->mobile = $request->mobile;
        $a->sponser_id = $request->sponcer_id;
        $a->plan = $request->plan;

        $a->save();


       return redirect()->route('admin.customer.temp.all');
    }
    
    
    public function DeleteTemp($id){
        $a = CreateCustomer::find($id);

        $a->delete();

        return back();
    }


    public function AllCustomerapiyttyygh(){


        {
            $customers = CreateCustomer::select([
                'id',
                'fname',
                'lname',
                'email',
                'mobile',
                'sponser_id',
                'hand',
                'is_mobile_verified'
            ])->orderBy('id', 'DESC');

            return DataTables::of($customers)
                ->addColumn('action', function ($invoice) {
                    return '<a href="' . route('admin.customer.temp.convert', $invoice->id) . '" class=" btn btn-xs btn-primary" title="View details"><i class="la la-edit"></i>Convert </a>';
                })

                 ->addColumn('edit', function ($invoice) {
                    return '<a href="' . route('admin.customer.temp.edit', $invoice->id) . '" class=" btn btn-xs btn-info" title="View details"><i class="la la-edit"></i>Edit </a>';
                })

                ->addColumn('delete', function ($invoice) {
                    return '<a href="' . route('admin.customer.temp.convert.delete', $invoice->id) . '" class=" btn btn-xs btn-danger" title="View details"><i class="la la-edit"></i>Delete </a>';
                })

                ->rawColumns(['action','delete','edit'])
                ->make(true);
        }

    }
}
