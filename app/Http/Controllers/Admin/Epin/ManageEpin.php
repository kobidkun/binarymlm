<?php

namespace App\Http\Controllers\Admin\Epin;

use App\Model\CreateEpin;
use App\Model\Customers\Customer;
use App\Model\Customers\CustomerToEpin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class ManageEpin extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:admin']);
    }



    public function CreateSingleview(Request $request){
        return view('pages.admin.epin.createepin');

    }

   public function CreateSingle(Request $request){


       $epinrand = str_random(12);

       $time = time() ;

       $epin = $epinrand.$time;




       $a = new CreateEpin();

       $a->customer_id = $request->customer_id;
       $a->value = $request->value;

       $a->epin = $epin;

       $a->is_used = 0;

       $a->save();

       //return back();

       return redirect()->route('admin.epin.all');




   }


    public function CreateMulview(Request $request){
        return view('pages.admin.epin.createmultiple');

    }


   public function CreateSinglemul(Request $request){


       for (   $i=1; $i<$request->no; $i++) {
           $epinrand = str_random(12);
           $time = time() ;
           $epin = $epinrand.$time;


           $a = new CreateEpin();

           $a->value = $request->value;
           $a->customer_id = $request->customer_id;
           $a->epin = $epin;
           $a->is_used = 0;
           $a->save();



       }


       return redirect()->route('admin.epin.all');





   }


    public function AllEpin(Request $request){
        return view('pages.admin.epin.all');

    }


    public function EpinApi(){

        $customers = CreateEpin::select([
            'id',
            'customer_id',
            'epin',
            'used_by',
            'is_used',
            'value',
        ])->orderBy('updated_at', 'DESC');

        return DataTables::of($customers)




            ->editColumn('customer_id', function ($invoice) {

              if ($invoice->customer_id !== null){
                  $a = Customer::where('id', $invoice->customer_id)->first();

                  return $a->fname.' '.$a->lname.' IC NO: '.$a->ic_number;
              } else return '';


            })




            ->rawColumns(['customer_id'])
            ->make(true);
    }



    public function AllEpinused(Request $request){
        return view('pages.admin.epin.allused');

    }


    public function EpinusedApi(){

        $customers = CustomerToEpin::select([
            'id',
            'customer_id',
            'epin',
            'used_by',
            'is_used',
            'value',
        ])->orderBy('updated_at', 'DESC');

        return DataTables::of($customers)




            ->editColumn('customer_id', function ($invoice) {

                if ($invoice->customer_id !== null){
                    $a = Customer::where('id', $invoice->customer_id)->first();

                    return $a->fname.' '.$a->lname.' IC NO: '.$a->ic_number;
                } else return '';


            })


            ->editColumn('is_used', function ($invoice) {



                    return 'Yes';



            })


            ->editColumn('used_by', function ($invoice) {

                if ($invoice->customer_id !== null){
                    $a = Customer::where('id', $invoice->used_by)->first();

                    return $a->fname.' '.$a->lname.' IC NO: '.$a->ic_number;
                } else return '';


            })




            ->rawColumns(['customer_id','used_by'])
            ->make(true);
    }





}
