<?php

namespace App\Http\Controllers\Admin\Misc;

use App\Model\Customers\Customer;
use App\Model\Customers\CustomerToEarningToTranactions;
use App\Model\Customers\CustomerToPayouts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Session;
class MiscManage extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin']);
    }


    public function TranactionsView(){

        return view('pages.admin.tranac.all');
    }

    public function TranactionsViewApi(){

        $customers = CustomerToEarningToTranactions::select([
            'id',
            'customer_id',
            'amount',
            'direct_income',
            'generation_income',
            'purpose',
            'payer_id',
            'updated_at',
        ])->orderBy('updated_at', 'DESC');

        return DataTables::of($customers)


            ->editColumn('customer_id', function ($invoice) {

                $a = Customer::find($invoice->customer_id);

                return $a->fname.' '.$a->lname.  ' / MM0000'.$a->ic_number;


            })

            ->editColumn('payer_id', function ($invoice) {

                $a = Customer::find($invoice->payer_id);

                return $a->fname.' '.$a->lname.  ' / MM0000'.$a->ic_number;


            })

            ->make(true);

    }




    public function PayoutView(){

        return view('pages.admin.tranac.allpayout');
    }

    public function PayoutViewnew(){

        return view('pages.admin.tranac.createpayout');
    }

    public function PayoutViewnewsave(Request $request){

       $a = new CustomerToPayouts();

       $a->customer_id = $request->customer_id;
       $a->amount = $request->amount;
       $a->medium = $request->medium;
       $a->save();

       return back();
    }

    public function PayoutViewApi(){

        $customers = CustomerToPayouts::select([
            'id',
            'customer_id',
            'amount',
            'medium',
            'updated_at',
        ])->orderBy('updated_at', 'DESC');

        return DataTables::of($customers)


            ->editColumn('customer_id', function ($invoice) {

                $a = Customer::find($invoice->customer_id);

                return $a->fname.' '.$a->lname.  ' / MM0000'.$a->ic_number;


            })


            ->make(true);

    }



    public function Roi(){
        return view('pages.admin.tranac.roi');
    }



    public function RoiSaveBinarySave(){


        if (\Carbon\Carbon::now()->day > 15){


                       $dateis = \Carbon\Carbon::now()->endOfMonth()->day;

                }


        else {
            $dateis =    '15th of this month';
        }




        Session::flash('message2', "You can only Perform this action on  ".$dateis."th of this month");

        return back();
    }

    public function RoiSave(){

        $isdate = \Carbon\Carbon::now()->endOfMonth();

        Session::flash('message', "You can only Perform this action on ".$isdate);
        return back();

    }






}
