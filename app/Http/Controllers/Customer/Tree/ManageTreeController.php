<?php

namespace App\Http\Controllers\Customer\Tree;

use App\Model\Customers\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ManageTreeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:customer,admin');
    }

    public function ShowTreeView(){

        $user = Auth::user();


        return view('pages.customer.tree.treeid')->with([
            'customer' => $user
        ]);
    }

    public function GetTreeApi($id){
        $findReqUserid = Customer::findorfail($id);

        //  dd($findReqUserid);



        $findReqUser = Customer::findorfail($findReqUserid->id);

        if ($findReqUserid->left_id != null){
            $leftWing = Customer::findorfail($findReqUserid->left_id);
        } else {
            $leftWing = null;
        }


        if ($findReqUserid->right_id != null){
            $rightWing = Customer::findorfail($findReqUserid->right_id);
        } else {
            $rightWing = null;
        }










        return response()->json(

            ['customer' => $findReqUser,
                'left' =>   $leftWing,
                'right' =>   $rightWing
            ]



        );
    }



    public function ViewTreeViewbyid($id){
        $findReqUserid = Customer::findorfail($id);

        return view('pages.customer.tree.treebyid')->with([
            'customer' => $findReqUserid,
            'id' => $id
        ]);




    }














}
