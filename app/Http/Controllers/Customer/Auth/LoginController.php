<?php

namespace App\Http\Controllers\Customer\Auth;

use App\Http\Requests\Front\Register;
use App\Model\Customers\Customer;
use App\Model\Temp\CreateCustomer;
use App\Model\Temp\CreateCustomerToVerify;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Session;
use Validator;

class LoginController extends Controller
{
    public function loginview(){

        return view('pages.customer.auth.login');

    }



    public function StoreLogint(Request $request){


        $email = $request->email;

    //    if (CreateCustomer::where('email', $email)->exists()){

         //   return 'your Payment is not Approved';
     //   }




        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput($request->only('email', 'remember'));
        }


        // Attempt to log the user in
        if (Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request->password],
            $request->remember)) {
            // if successful, then redirect to their intended location
            return redirect()->intended(route('customer.dashboard'));
        }
        // if unsuccessful, then redirect back to the login with the form data
        return redirect() ->back()
            ->withErrors($validator, 'password')
            ->withInput($request->only('email', 'remember'));

    }





    public function StoreLogin(Request $request){


        $email = $request->email;

    //    if (CreateCustomer::where('email', $email)->exists()){

         //   return 'your Payment is not Approved';
     //   }




        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput($request->only('email', 'remember'));
        }


        // Attempt to log the user in
        if (Auth::guard('customer')->attempt(['ic_number' => $request->email, 'password' => $request->password],
            $request->remember)) {
            // if successful, then redirect to their intended location
            return redirect()->intended(route('customer.tree.view'));
        }
        // if unsuccessful, then redirect back to the login with the form data
        return redirect() ->back()
            ->withErrors($validator, 'password')
            ->withInput($request->only('email', 'remember'));

    }


    protected function credentials(Request $request)
    {
        $field = $this->field($request);

        return [
            $field => $request->get($this->username()),
            'password' => $request->get('password'),
        ];
    }







    public function field(Request $request)
    {
        $email = $this->ic_number();

        return filter_var($request->get($email), FILTER_VALIDATE_EMAIL) ? $email : 'ic_number';
    }


    protected function validateLogin(Request $request)
    {
        $field = $this->field($request);

        $messages = ["{$this->ic_number()}.exists" => 'The account you are trying to login is not registered or it has been disabled.'];

        $this->validate($request, [
            $this->ic_number() => "required|exists:customers,{$field}",
            'password' => 'required',
        ], $messages);
    }

    public function logout()
    {
        Auth::guard('customer')->logout();
        return redirect(route('customer.login.view'));
    }

    public function RegisterTemp(Register $request){



      /*  if ($request->hand === 'left'){

            $findupline = Customer::where('ic_number', $request->sponcer_id)->first();

            if ($findupline->left_id !== null){

                Session::flash('message', "Left Member Already Exists Already Exists Please use Different Sponcer Id");
                return  back()->with(
                    'errorpost',  ['Left Member Already Exists Already Exists Please use Different Sponcer Id']
                );
            }


        } else if ($request->hand === 'right'){

            $findupline = Customer::where('ic_number', $request->sponcer_id)->first();

            if ($findupline->right_id !== null){

                Session::flash('message', "Right Member Already Exists Already Exists Please use Different Sponcer Id");

                return  back()->with(
                    'errorpost',  ['Right Member Already Exists Already Exists Please use Different Sponcer Id']
                );
            }


        }

       // if ()






        $s = new CreateCustomer();
        $s->fname = $request->fname;
        $s->lname = $request->lname;
        $s->email = $request->email;
        $s->mobile = $request->mobile;
        $s->phone = $request->phone;
        $s->hand = $request->hand;
        $s->sponser_id = $request->sponcer_id;
        $s->is_mobile_verified = 'no';
        $s->is_email_verified = 'no';
        $s->password = bcrypt('password');
        $s->save();



        ///
        ///

        $six_digit_random_number = mt_rand(100000, 999999);


        $p = new CreateCustomerToVerify();
        $p->customer_id = $s->id;
        $p->otp_verify = $six_digit_random_number;
        $p->save();

        $smssmg = 'Hi '.$s->fname.' your registration is successful  login with the your email '.$s->email.' login password which is password Thanks'.env('APP_NAME');

       // send sms

        //Your authentication key
        $authKey = "143565AIE7jf1Mb5abd98a0";

//Multiple mobiles numbers separated by comma
        $mobileNumber = $request->mobile;

//Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "MULTST";

//Your message to send, Add URL encoding here.
        $message = urlencode($smssmg );

//Define route
        $route = "4";
//Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

//API URL
        $url="http://api.msg91.com/api/sendhttp.php";

// init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


//Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


//get response
        $output = curl_exec($ch);

//Print error if any
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);*/



       // $cust = CreateCustomer::find($id);

        if ($request->sponser_id != null ){
            $getlastrecord = Customer::orderBy('created_at', 'desc')->first();

            $icnum = $getlastrecord->ic_number +1;

        } else {

            $icnum = 1;
        }










        $s = new Customer();
        $s->fname = $cust->fname;
        $s->lname = $cust->lname;
        $s->ic_number = $icnum ;
        $s->g_name = $cust->g_name;
        $s->commission = $cust->commission;
        $s->dob = $cust->dob;
        $s->sex = $cust->sex;
        $s->ref = $cust->ref;
        $s->hand = $cust->hand;
        $s->plan = $cust->plan;
        //   $s->right_ic_number = $cust->right_ic_number;
        // $s->left_ic_number = $cust->left_ic_number;
        //  $s->right_id = $cust->right_id;
        //   $s->left_id = $cust->left_id;
        $s->sponser_id = $cust->sponser_id;
        $s->street = $cust->street;
        $s->locality = $cust->locality;
        $s->city = $cust->city;
        $s->state = $cust->state;
        $s->country = $cust->country;
        $s->pin = $cust->pin;
        $s->permanent_address = $cust->permanent_address;
        $s->supply_address = $cust->supply_address;
        $s->email = $cust->email;
        $s->mobile = $cust->mobile;
        $s->phone = $cust->phone;
        $s->nominee_name = $cust->nominee_name;
        $s->nominee_relation = $cust->nominee_relation;
        $s->nominee_address = $cust->nominee_address;
        $s->nominee_dob = $cust->nominee_dob;
        $s->bank_name = $cust->bank_name;
        $s->bank_ifsc = $cust->bank_ifsc;
        $s->bank_micr = $cust->bank_micr;
        $s->bank_account_holder_name = $cust->bank_account_holder_name;
        $s->bank_account_number = $cust->bank_account_number;
        $s->bank_branch = $cust->bank_branch;
        $s->pan = $cust->pan;
        $s->aadhar = $cust->aadhar;
        $s->gstin = $cust->gstin;
        $s->gstin_state = $cust->gstin_state;
        $s->billing_name = $cust->billing_name;
        $s->password = bcrypt($cust->password);
        $s->save();

        //  dd($s->id);


        $custcreateid = $s->id;




        $data = $s;

        $customertocust = new CustomerToCustomer();

        $customertocust->customer_id  = $s->id;
        $customertocust->ic_number  = $s->ic_number;
        $customertocust->customer_parient_ic_number  = $s->sponser_id;


        $customertocust->hand  = $s->hand;

        $customertocust->save();


        if ($cust->sponser_id !== null){





            //update customer uplone;

            $findupline = Customer::where('ic_number', $data->sponser_id)->first();

            if ($cust->hand === 'right'){

                $findupline->right_id = $s->id;
                $findupline->right_ic_number = $s->ic_number;

                $updatecusttocust = CustomerToCustomer::where('customer_id', $data->id)->first();

                $updatecusttocust->right_ic_number = $s->ic_number;
                $updatecusttocust->right_id = $s->id;

                $updatecusttocust->save();


            } elseif ($cust->hand === 'left'){

                $findupline->left_id = $s->id;
                $findupline->left_ic_number = $s->ic_number;


                $updatecusttocust = CustomerToCustomer::where('customer_id', $data->id)->first();


                $updatecusttocust->left_ic_number = $s->ic_number;
                $updatecusttocust->left_id = $s->id;

                $updatecusttocust->save();

            }

            $findupline->save();

            //  if

            //
        }


        ////sms
        ///
        ///
        ///


        $smssmg = 'Hi '.$s->fname.' your payment has been approved and you can login with the your login password which is password Thanks'.env('APP_NAME');

        // send sms

        //Your authentication key
        $authKey = "143565AIE7jf1Mb5abd98a0";

//Multiple mobiles numbers separated by comma
        $mobileNumber = $s->mobile;

//Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "MULTST";

//Your message to send, Add URL encoding here.
        $message = urlencode($smssmg );

//Define route
        $route = "4";
//Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

//API URL
        $url="http://api.msg91.com/api/sendhttp.php";

// init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


//Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


//get response
        $output = curl_exec($ch);

//Print error if any
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);






        return redirect(route('front.success'));
    }



}
