<?php

namespace App\Http\Controllers\Customer\Profile;

use App\Model\CreateEpin;
use App\Model\Customers\Customer;
use App\Model\Customers\CustomerToEarning;
use App\Model\Customers\CustomerToEarningToTranactions;
use App\Model\Customers\CustomerToEpin;
use App\Model\Customers\CustomerToFiles;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ManageProfile extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:customer');
    }

    public function Dashboard(){

        $customer = Customer::find(Auth::user()->id);



        $lastmonthtxts =  $customer->customer_to_earning_to_tranactionsLastmonth;
        $thismonthtxts =  $customer->customer_to_earning_to_tranactionsThismonth;
        $allmonthtxts =  $customer->customer_to_earning_to_tranactions;
           // ->where('MONTH(created_at) = ?',[$currentMonth]);

        $totalincome = $customer->customer_to_earnings;
        $payouts= $customer->customer_to_payouts;




       // return   dd($totalincome);





        return view('pages.customer.pages.dashboard')->with([
            'lmtx' => $lastmonthtxts,
            'lmtxi' => $lastmonthtxts->sum('amount'),
            'tmtx' => $thismonthtxts,
            'tmtxi' => $thismonthtxts->sum('amount'),
            'amtx' => $allmonthtxts,
            'amtxi' => $allmonthtxts->sum('amount'),
            'income' => $totalincome,
            'payouts' => $payouts,
        ]);
    }

    public function ViewProfile(){


        $c = Auth::user();

        return view('pages.customer.pages.profile')->with([
            'customer'=> $c
        ]);

    }

    public function Payoutview(){


        $user = Auth::user()->id;

        $cust = Customer::find($user);

        $payout = $cust->customer_to_earnings;

///
   //     dd($payout);



        return view('pages.customer.pages.createpayout')->with(
            [
                'payout' => $payout
            ]
        );
    }

    public function SaveProfile(Request $request){

        $s  = Auth::user();

        // return $customer;


        $s->fname = $request->fname;
        $s->lname = $request->lname;

        $s->g_name = $request->g_name;
        $s->commission = $request->commission;
        $s->dob = $request->dob;
        $s->sex = $request->sex;
        $s->street = $request->street;
        $s->locality = $request->locality;
        $s->city = $request->city;
        $s->state = $request->state;
        $s->country = $request->country;
        $s->pin = $request->pin;
        $s->permanent_address = $request->permanent_address;
        $s->supply_address = $request->supply_address;
        $s->email = $request->email;
        $s->mobile = $request->mobile;
        $s->phone = $request->phone;
        $s->nominee_name = $request->nominee_name;
        $s->nominee_relation = $request->nominee_relation;
        $s->nominee_address = $request->nominee_address;
        $s->nominee_dob = $request->nominee_dob;
        $s->bank_name = $request->bank_name;
        $s->bank_ifsc = $request->bank_ifsc;
        $s->bank_micr = $request->bank_micr;
        $s->bank_account_holder_name = $request->bank_account_holder_name;
        $s->bank_account_number = $request->bank_account_number;
        $s->bank_branch = $request->bank_branch;
        $s->pan = $request->pan;
        $s->aadhar = $request->aadhar;
        $s->gstin = $request->gstin;
        $s->gstin_state = $request->gstin_state;
        $s->billing_name = $request->billing_name;
        $s->save();

        return back();

    }

    public function ChangePassword(Request $request){

        $c = Auth::user();

        return view('pages.customer.pages.password')->with([
            'customer'=> $c
        ]);

    }

    public function ChangePasswordSave(Request $request){

        $customer  = Auth::user();

        // return $customer;

        $customer->password = bcrypt($request->password);

        $customer->save();

        return back();

    }



    public function UploadAAdhar(Request $request){
        if ( $request->img  === null ){
            return 'No  image ';
        }


        $imglocname = time() . '-' . $request->img->getClientOriginalName();
        $imglocnamepath = '/images/files/aadhar/' . $imglocname;

        $imgloc = $request->img->storeAs('public/images/files/aadhar', $imglocname);


        if(@is_array(getimagesize($imgloc))){
            $image = true;
        } else {
            $image = false;
        }






        $img = new CustomerToFiles();

        $img->customer_id = $request->customer_id;
        $img->is_img = 'yes';
        $img->file_name = 'aadhar';
        $img->path = $imglocnamepath;

        $img->save();



        return back();
    }


    public function UploadPan(Request $request){
        if ( $request->img  === null ){
            return 'No  image ';
        }


        $imglocname = time() . '-' . $request->img->getClientOriginalName();
        $imglocnamepath = '/images/files/pan/' . $imglocname;

        $imgloc = $request->img->storeAs('public/images/files/pan', $imglocname);


        if(@is_array(getimagesize($imgloc))){
            $image = true;
        } else {
            $image = false;
        }






        $img = new CustomerToFiles();

        $img->customer_id = $request->customer_id;
        $img->is_img = 'yes';
        $img->file_name = 'pan';
        $img->path = $imglocnamepath;

        $img->save();



        return back();
    }


    public function ActivatebyEpin(){

        return view('pages.customer.epin.epin');

    }


    public function VerifybyEpin(Request $request){

        $epin = $request->epin;

        $a = CreateEpin::where('epin', $epin)->first();


        if ($a !== null){


            $d = new CustomerToEpin();

            $d->customer_id = $a->customer_id;
            $d->epin = $a->epin;
            $d->used_by = Auth::user()->id;
            $d->is_used = 1;
            $d->value = $a->value;

            $d->save();


            $c = Customer::find(Auth::user()->id);
            $c->plan = $a->value;
            $c->save();



        //    $a->delete();


            $this->DirectPropfit($c);


        return redirect()->route('customer.dashboard');


        } else {
            return 'Your Epin is invalid';
        }





    }


    public function DirectPropfit($c){

        $customer = $c;

        $sponcer = Customer::where('ic_number', $c->sponser_id)->first();

        if ($sponcer->left_id !== null && $sponcer->right_id !== null){

           $cust = Customer::find($sponcer->id);

          // dd($cust);



            if ($cust->sponser_id !== null){

                $chktree = $this->CheckTree($cust);

            } else {
                $chktree = true;
            }






          if ($chktree === true){

              $planamt = $this->TakeLowerAmount($customer);

              if ($cust->customer_to_earnings === null){


                  //calcu

                  $amt = $planamt*0.10;




                  //calcu end

                  $a = new CustomerToEarning();

                  if ($cust->hand === 'left'){
                      $a->leftincome = $c->plan;
                  }

                  if ($cust->hand === 'right'){

                      $a->rightincome = $c->plan;
                  }

                  $a->customer_id = $cust->id;
                  $a->amount = $amt;
                  $a->direct_income = $amt;
                  $a->generation_income = 0;
                  $a->rate = 0.08;
                  $a->save();

                  $b = new CustomerToEarningToTranactions();
                  $b->customer_id = $cust->id;
                  $b->amount = $amt;
                  $b->direct_income = $amt;
                  $b->generation_income = 0;
                  $b->purpose = 'Direct Purchase';
                  $b->payer_id = Auth::user()->id;

                  $b->save();


                  $customersponcer = $cust->sponser_id;

                  $comission = $amt;

                  $this->UpdateSecondGeneration($customersponcer , $comission, $amt);


              }
              elseif ($cust->customer_to_earnings !== null){

                  $amt = $planamt*0.10;



                  $a = CustomerToEarning::where('customer_id',$cust->id)->first();

                  $a->amount = $a->amount + $amt;
                  $a->direct_income = $a->direct_income + $amt;

                  if ($cust->hand === 'left'){
                      $a->leftincome = $a->leftincome+$c->plan;
                  }

                  if ($cust->hand === 'right'){

                      $a->rightincome = $a->rightincome+$c->plan;
                  }

                  $a->save();


                  $b = new CustomerToEarningToTranactions();
                  $b->customer_id = $cust->id;
                  $b->amount = $amt;
                  $b->direct_income = $amt;
                  $b->generation_income = 0;
                  $b->purpose = 'Direct Purchase';
                  $b->payer_id = Auth::user()->id;

                  $b->save();


                  $customersponcer = $cust->sponser_id;

                  $comission = $amt;

                  //  dd($comission);

                  if ($sponcer->generation > 1 ){

                      $secgen =   $this->UpdateSecondGeneration($customersponcer , $comission, $amt);

                  }



                  // dd($secgen[0]);


                  if ($sponcer->generation > 2 && $secgen !== false) {
                      $secgen2 = $this->UpdateSecondGeneration($secgen[0], $secgen[1], $secgen[2]);
                  }

                  if ($sponcer->generation > 3 && $secgen2 !== false) {

                      $secgen3 =  $this->UpdateSecondGeneration($secgen2[0], $secgen2[1], $secgen2[2]);

                  }


                  if ($sponcer->generation > 4 && $secgen3 !== false) {

                      $secgen4 =  $this->UpdateSecondGeneration($secgen3[0], $secgen3[1], $secgen3[2]);

                  }

                  if ($sponcer->generation > 5 && $secgen4 !== false) {

                      $secgen5 =  $this->UpdateSecondGeneration($secgen4[0], $secgen4[1], $secgen4[2]);

                  }

                  if ($sponcer->generation > 6 && $secgen5 !== false) {

                      $secgen6 =  $this->UpdateSecondGeneration($secgen5[0], $secgen5[1], $secgen5[2]);

                  }

                  if ($sponcer->generation > 7 && $secgen6 !== false) {

                      $secgen7 =  $this->UpdateSecondGeneration($secgen6[0], $secgen6[1], $secgen6[2]);

                  }

                  if ($sponcer->generation > 8 && $secgen7 !== false) {

                      $secgen8 =  $this->UpdateSecondGeneration($secgen7[0], $secgen7[1], $secgen7[2]);

                  }

                  if ($sponcer->generation > 9 && $secgen8 !== false) {

                      $secgen9 =  $this->UpdateSecondGeneration($secgen8[0], $secgen8[1], $secgen8[2]);

                  }

                  if ($sponcer->generation > 10 && $secgen9 !== false) {

                      $secgen10 =  $this->UpdateSecondGeneration($secgen9[0], $secgen9[1], $secgen9[2]);

                  }










              }
          } else {

              return false;
          }







        }



    }


    public function UpdateSecondGeneration($customersponcer , $comission , $plan){



        $getcustomer = Customer::where('ic_number',$customersponcer)->first();


        if ($getcustomer->sponser_id !== null){

            $chktree = $this->CheckTree($getcustomer);

        } else {
            $chktree = true;
        }



      if ($chktree === true){



          if ($getcustomer->customer_to_earnings === null){


              //calcu

              $amt = $comission*0.10;

            //  dd($amt);




              //calcu end

              $a = new CustomerToEarning();

              $a->customer_id = $getcustomer->id;
              $a->amount = $amt;
              $a->direct_income = $amt;
              $a->generation_income = 0;
              $a->rate = 0.08;

              if ($getcustomer->hand === 'left'){
                  $a->leftincome = $getcustomer->plan;
              }

              if ($getcustomer->hand === 'right'){

                  $a->rightincome = $getcustomer->plan;
              }






              $a->save();

              $b = new CustomerToEarningToTranactions();
              $b->customer_id = $getcustomer->id;
              $b->amount = $amt;
              $b->direct_income = $amt;
              $b->generation_income = 0;
              $b->purpose = 'Direct Purchase';
              $b->payer_id = Auth::user()->id;

              $b->save();


              return [

                  0 => $getcustomer->sponser_id,
                  1 => $amt,
                  2 => $plan
              ];







          } elseif ($getcustomer->customer_to_earnings !== null){

              $amt = $comission*0.10;

            //  dd($amt);



              $a = CustomerToEarning::where('customer_id',$getcustomer->id)->first();

              $a->amount = $a->amount + $amt;
              $a->direct_income = $a->direct_income + $amt;
              if ($getcustomer->hand === 'left'){
                  $a->leftincome = $a->leftincome+$getcustomer->plan;
              }

              if ($getcustomer->hand === 'right'){

                  $a->rightincome = $a->rightincome+$getcustomer->plan;
              }

              $a->save();


              $b = new CustomerToEarningToTranactions();
              $b->customer_id = $getcustomer->id;
              $b->amount = $amt;
              $b->direct_income = $amt;
              $b->generation_income = 0;
              $b->purpose = 'Direct Purchase';
              $b->payer_id = Auth::user()->id;

              $b->save();

              return [

                  0 => $getcustomer->sponser_id,
                  1 => $amt,
                  2 => $plan
              ];







          }

      } else {

          return false;
      }





    }


    public function CheckTree($getcustomer){

        $getCustomert = DB::table('customers')
            ->where('ic_number',$getcustomer->sponser_id)
            ->first();

        //  dd($getcustomer->sponser_id);

        if ($getCustomert->left_id !== null && $getCustomert->right_id !== null){

            //check active

            $lc = Customer::find($getCustomert->left_id);

            $rc = Customer::find($getCustomert->right_id);

            if ( $lc->plan !== null && $rc->plan !== null){

                return  true;

            }




        } else {
           return false;
        }
    }



    public function TakeLowerAmount($getcustomer){



        $getCustomert = DB::table('customers')
            ->where('ic_number',$getcustomer->sponser_id)
            ->first();


      //  dd($getCustomert);

        $lc = Customer::find($getCustomert->left_id);

        $rc = Customer::find($getCustomert->right_id);


        if ($lc > $rc){

            return $rc->plan*2;


        } elseif ($rc > $lc){

            return $lc->plan*2;

        }




    }














}
