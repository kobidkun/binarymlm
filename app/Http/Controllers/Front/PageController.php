<?php

namespace App\Http\Controllers\Front;

use App\Model\Customers\CustomerToCustomer;
use App\Model\Temp\CreateCustomer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Model\Customers\Customer;


class PageController extends Controller
{
    public function HomePage(){
        return view('front.pages.myhome');
    }

    public function About(){
        return view('front.pages.about');
    }

    public function Returns(){
        return view('front.pages.returns');
    }


    public function Success(){
        return view('front.pages.success');
    }

    public function OTP($id){

        $cust = CreateCustomer::findorfail($id);

        return view('front.pages.otp')->with([
            'customer' => $cust
        ]);
    }


    public function OTPVerify(Request $request){

        $cust = CreateCustomer::findorfail($request->customer_id);

        $getOTP = $cust->create_customer_to_verifies;

        $otp = $request->otp0.$request->otp1.$request->otp2.$request->otp3.$request->otp4.$request->otp5;

        if ($otp === $getOTP->otp_verify){


            $cust->is_mobile_verified = 'Yes';

            $cust->save();


            return redirect(route('front.success'));

        } else {

            Session::flash('message', "You have entered invalid OTP");
            return back();
        }


    }

    public function SignUp(){
        return view('front.pages.signup')->with([
            'errorpost' => null
        ]);;
    }
    
        public function GetAllCustomerviaSearch(Request $request){






        $query = $request->get('term','');

        $customers = Customer::where('fname', 'LIKE', '%' . $query . '%')
            ->orwhere('ic_number', 'LIKE', '%' . $query . '%')

            ->take(5)->get();

        $data=array();
        foreach ($customers as $customer) {
            $data[]=array(
                'value'=>$customer->fname .' '. $customer->lname,
                'lname'=>$customer->lname,
                'id'=>$customer->id,
                'ic_number'=>$customer->ic_number,
                'gstin'=>$customer->gstin,
                'street'=>$customer->street,
                'locality'=>$customer->locality,
                'city'=>$customer->city,
                'state'=>$customer->state,
                'country'=>$customer->country,
                'pin' => $customer->pin,
                'permanent_address' => $customer->permanent_address,
                'email' => $customer->email,
                'mobile' => $customer->mobile,
                'pan' => $customer->pan,
                'gstin_state' => $customer->gstin_state,
                'billing_name' => $customer->billing_name,
                'commission' => $customer->commission,
                'left_id' => $customer->left_id,
                'right_id' => $customer->right_id,
                'right_ic_number' => $customer->right_ic_number,
                'left_ic_number' => $customer->left_ic_number,

            );
        }
        if(count($data))
            return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }



    public function CreateCustomerfront(Request $request ){











        if ($request->sponser_id === null ){

            $icnum = 1;
            $gen= 1;
        } else {
            $getlastrecord = Customer::orderBy('created_at', 'desc')->first();
            $getlastrecordgen = Customer::where('ic_number','=', $request->sponser_id)->first();

          //  dd($getlastrecordgen);

            $icnum = $getlastrecord->ic_number +1;
            $gen = $getlastrecordgen->generation +1;




            if ($request->hand === 'left'){

                $findupline = Customer::where('ic_number', $request->sponser_id)->first();

              //  dd($findupline);

                if ($findupline->left_id !== NULL){

                    Session::flash('message', "Left Member Already Exists Already Exists Please use Different Sponcer Id");
                    return  back()->with(
                        'errorpost',  ['Left Member Already Exists Already Exists Please use Different Sponcer Id']
                    );
                }


            } else if ($request->hand === 'right'){

                $findupline = Customer::where('ic_number', $request->sponser_id)->first();

                if ($findupline->right_id !== NULL){

                    Session::flash('message', "Right Member Already Exists Already Exists Please use Different Sponcer Id");

                    return  back()->with(
                        'errorpost',  ['Right Member Already Exists Already Exists Please use Different Sponcer Id']
                    );
                }


            }

           // return $icnum;

        }














        $s = new Customer();
        $s->fname = $request->fname;
        $s->lname = $request->lname;
        $s->ic_number = $icnum ;
        $s->generation = $gen ;
        $s->g_name = $request->g_name;
        $s->commission = $request->commission;
        $s->dob = $request->dob;
        $s->sex = $request->sex;
        $s->ref = $request->ref;
        $s->hand = $request->hand;
        //   $s->right_ic_number = $request->right_ic_number;
        // $s->left_ic_number = $request->left_ic_number;
        //  $s->right_id = $request->right_id;
        //   $s->left_id = $request->left_id;
        $s->sponser_id = $request->sponser_id;
        $s->street = $request->street;
        $s->locality = $request->locality;
        $s->city = $request->city;
        $s->state = $request->state;
        $s->country = $request->country;
        $s->pin = $request->pin;
        $s->permanent_address = $request->permanent_address;
        $s->supply_address = $request->supply_address;
        $s->email = $request->email;
        $s->mobile = $request->mobile;
        $s->phone = $request->phone;
        $s->nominee_name = $request->nominee_name;
        $s->nominee_relation = $request->nominee_relation;
        $s->nominee_address = $request->nominee_address;
        $s->nominee_dob = $request->nominee_dob;
        $s->bank_name = $request->bank_name;
        $s->bank_ifsc = $request->bank_ifsc;
        $s->bank_micr = $request->bank_micr;
        $s->bank_account_holder_name = $request->bank_account_holder_name;
        $s->bank_account_number = $request->bank_account_number;
        $s->bank_branch = $request->bank_branch;
        $s->pan = $request->pan;
        $s->aadhar = $request->aadhar;
        $s->gstin = $request->gstin;
        $s->gstin_state = $request->gstin_state;
        $s->billing_name = $request->billing_name;
        $s->password = bcrypt('password');
        $s->save();

        //  dd($s->id);




        //sms



        $smssmg = 'Hi '.$request->fname.', your Member ID is '.$icnum.'  you can login your account with your user name '.$request->email.'  and password is password  Thanks '.env('APP_NAME');

        // send sms

        //Your authentication key
        $authKey = "143565AIE7jf1Mb5abd98a0";

//Multiple mobiles numbers separated by comma
        $mobileNumber = $s->mobile;

//Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "MULTST";

//Your message to send, Add URL encoding here.
        $message = urlencode($smssmg );

//Define route
        $route = "4";
//Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

//API URL
        $url="http://api.msg91.com/api/sendhttp.php";

// init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


//Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


//get response
        $output = curl_exec($ch);

//Print error if any
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);








        $custcreateid = $s->id;




        $data = $s;

        $customertocust = new CustomerToCustomer();

        $customertocust->customer_id  = $s->id;
        $customertocust->ic_number  = $s->ic_number;
        $customertocust->customer_parient_ic_number  = $s->sponser_id;


        $customertocust->hand  = $s->hand;

        $customertocust->save();


        if ($request->sponser_id !== null){





            //update customer uplone;

            $findupline = Customer::where('ic_number', $data->sponser_id)->first();

            if ($request->hand === 'right'){

                $findupline->right_id = $s->id;
                $findupline->right_ic_number = $s->ic_number;

                $updatecusttocust = CustomerToCustomer::where('customer_id', $data->id)->first();

                $updatecusttocust->right_ic_number = $s->ic_number;
                $updatecusttocust->right_id = $s->id;

                $updatecusttocust->save();


            } elseif ($request->hand === 'left'){

                $findupline->left_id = $s->id;
                $findupline->left_ic_number = $s->ic_number;


                $updatecusttocust = CustomerToCustomer::where('customer_id', $data->id)->first();


                $updatecusttocust->left_ic_number = $s->ic_number;
                $updatecusttocust->left_id = $s->id;

                $updatecusttocust->save();

            }

            $findupline->save();

            //  if

            //
        }









        return redirect()->route('front.success');
    }




}
