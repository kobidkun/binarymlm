<?php

namespace App\Model\Temp;


use App\Model\Customers\Customer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class CreateCustomer extends Model
{

    public function create_customer_to_verifies()
    {
        return $this->hasOne('App\Model\Temp\CreateCustomerToVerify',
            'customer_id','id');
    }



}
