<?php

namespace App\Model\Customers;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    use Notifiable;

    protected $guard = 'customer';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'ic_number'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */



    public function customer_to_customers()
    {
        return $this->hasOne('App\Model\Customers\CustomerToCustomer',
            'customer_id','id');
    }

    public function customer()
    {
        return $this->hasOne('App\Model\Customers\Customer',
            'sponser_id','ic_number');
    }

    public function customer_to_profile_pictures()
    {
        return $this->hasOne('App\Model\Customers\CustomerToProfilePicture',
            'customer_id','id');
    }

    public function customer_to_files()
    {
        return $this->hasMany('App\Model\Customers\CustomerToFiles',
            'customer_id','id');
    }

    public function customer_to_epins()
    {
        return $this->hasOne('App\Model\Customers\CustomerToEpin',
            'used_by','id');
    }


    public function sponcer()
    {
        return $this->hasOne('App\Model\Customers\Customer',
            'ic_number','sponser_id');
    }


    public function customer_to_earnings()
    {
        return $this->hasOne('App\Model\Customers\CustomerToEarning',
            'customer_id','id');
    }

    public function customer_to_earning_to_tranactionsThismonth()
    {
        return $this->hasMany('App\Model\Customers\CustomerToEarningToTranactions',
            'customer_id','id')->whereMonth('created_at', Carbon::now()->month);
    }

    public function customer_to_earning_to_tranactionsLastmonth()
    {
        return $this->hasMany('App\Model\Customers\CustomerToEarningToTranactions',
            'customer_id','id')
            ->whereMonth( 'created_at', '=', Carbon::now()->subMonth()->month);
    }

    public function customer_to_earning_to_tranactions()
    {
        return $this->hasMany('App\Model\Customers\CustomerToEarningToTranactions',
            'customer_id','id');
    }


       public function customer_to_payouts()
    {
        return $this->hasMany('App\Model\Customers\CustomerToPayouts',
            'customer_id','id');
    }


}
